import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:localstorage/localstorage.dart';
import 'package:uni_advisor/services/microwave_service.dart';
import 'package:uni_advisor/services/preference_manager.dart';
import 'package:uni_advisor/view/hamburger_menu.dart';
import 'package:uni_advisor/viewModel/bloc/blocMicrowaves/microwave_bloc.dart';
import 'package:uni_advisor/viewModel/cubit/cubitConnectivity/connectivity_cubit.dart';

import '../configs/colors.dart';
import '../model/models/microwave.dart';
import '../model/models/user.dart';

class MicrowaveView extends StatefulWidget {
  @override
  _MicrowaveView createState() => _MicrowaveView();
}

class _MicrowaveView extends State<MicrowaveView> {
  List cards = [];
  late User user;
  final LocalStorage storage = LocalStorage('localstorage_app');

  final services = MicrowaveService();
  final double borderRadiusInt = 10;
  List<bool> showLikeIconTime = [];

  Future<void> findUserWithPereferences() async {
    var json_user = await PreferenceManager.getUser();
    user = User.fromJson(json_user!);
  }

  @override
  void initState() {
    findUserWithPereferences();
    cards = [];
    BlocProvider.of<MicrowaveBloc>(context).add(RequestAllMicrowaves());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
        drawer: const Drawer(child: HamburgerMenu()),
        appBar: AppBar(
          title: const Text("Localizador De Microondas"),
          backgroundColor: AppColors.appBarColor,
        ),
        body: BlocListener<ConnectivityCubit, ConnectivityState>(
            listener: (context, state) {
          if (state is InternetDisconnected) {
            ScaffoldMessenger.of(context).clearSnackBars();
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  children: const [
                    Expanded(
                      child: Text(
                        "Estas viendo información desactualizada sobre los  Microondas",
                        overflow:
                            TextOverflow.clip, // Customize overflow behavior
                        softWrap: true,
                      ),
                    ),
                  ],
                ),
                duration: const Duration(days: 365),
              ),
            );
          } else {
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
          }
        }, child: BlocBuilder<MicrowaveBloc, MicrowaveState>(
                builder: (((context, state) {
          if (state is AllMicrowavesObtained) {
            cards = (state.microwaves);
          }
          if (state is MicrowaveLoading) {
            return Center(
              child: Column(
                children: const [
                  SizedBox(height: 100),
                  CircularProgressIndicator(), // Agrega un icono de carga
                  SizedBox(
                      height:
                          20), // Agrega un espacio entre el icono y el texto
                  Text('Cargando... Espere un Momento'),
                ],
              ),
            );
          }

          return Center(child: basicView());
        }))))));
  }

  Widget basicView() {
    return SingleChildScrollView(
      child: SizedBox(
        height:
            MediaQuery.of(context).size.height - 100, // Set a height constraint
        width: MediaQuery.of(context).size.width - 20,
        child: ListView.separated(
          itemCount: cards.length,
          itemBuilder: (context, index) {
            Microwave micro = cards[index];
            bool showLikeIcon = micro.likes.contains(user.email);
            showLikeIconTime.add(false);

            return GestureDetector(
              onTap: () {
                PreferenceManager.saveMicrowave(micro.toJson());
                Navigator.of(context).pushNamed("/detailMicrowave");
              },
              onDoubleTap: () {
                showLikeIconTime[index] = true;
                print(showLikeIcon);
                if (showLikeIcon) {
                  services.deleteLike(micro.id, user.email);
                  micro.likes.remove(user.email);
                  showLikeIcon = false;

                  setState(() {});
                } else {
                  services.addLike(micro.id, user.email);
                  micro.likes.add(user.email);
                  setState(() {
                    showLikeIcon = true;
                    showLikeIconTime[index] = showLikeIcon;
                  });
                }
                Timer(const Duration(seconds: 1), () {
                  setState(() {
                    showLikeIconTime[index] = false;
                  });
                });
              },
              child: Stack(
                children: [
                  Card(
                    color: AppColors.microwavecolor,
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(borderRadiusInt),
                    ),
                    child: SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Material(
                                    color: AppColors.microwavecolor,
                                    elevation: 1,
                                    shadowColor:
                                        AppColors.primaryText.withOpacity(0.4),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(borderRadiusInt),
                                      topRight:
                                          Radius.circular(borderRadiusInt),
                                      bottomLeft:
                                          Radius.circular(borderRadiusInt),
                                      bottomRight:
                                          Radius.circular(borderRadiusInt),
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.only(
                                        topLeft:
                                            Radius.circular(borderRadiusInt),
                                        topRight:
                                            Radius.circular(borderRadiusInt),
                                        bottomLeft:
                                            Radius.circular(borderRadiusInt),
                                        bottomRight:
                                            Radius.circular(borderRadiusInt),
                                      ),
                                      child: CachedNetworkImage(
                                        imageUrl: micro.image,
                                        placeholder: (context, url) =>
                                            const CircularProgressIndicator(),
                                        errorWidget: (context, url, error) =>
                                            const Icon(Icons.error),
                                        width: 140,
                                        height: 160,
                                        fit: BoxFit.fitHeight,
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Text(
                                            "Likes: ${micro.likes.length}",
                                            style: TextStyle(
                                                color: AppColors.negativeSpace),
                                          ),
                                          Icon(
                                            Icons.thumb_up,
                                            size: 24,
                                            color: showLikeIcon
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Text('Edificio: ',
                                          style: TextStyle(
                                              color: AppColors.negativeSpace)),
                                      Text(micro.edifice,
                                          style: TextStyle(
                                              color: AppColors.negativeSpace)),
                                      Text('Cantidad: ${micro.quantity}',
                                          style: TextStyle(
                                              color: AppColors.negativeSpace)),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          'Horario Disponible: ${micro.empy_schedule}',
                                          style: TextStyle(
                                              color: AppColors.negativeSpace)),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                          'Horario Concurrido: ${micro.busy_schedule}',
                                          style: TextStyle(
                                              color: AppColors.negativeSpace)),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text('Detalles del lugar: ',
                                          style: TextStyle(
                                              color: AppColors.negativeSpace)),
                                      Text(' -  ${micro.descriptive_location}',
                                          style: TextStyle(
                                              color: AppColors.negativeSpace)),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 250,
                    child: Center(
                      child: AnimatedOpacity(
                        opacity:
                            showLikeIcon && showLikeIconTime[index] ? 1.0 : 0.0,
                        duration: const Duration(milliseconds: 300),
                        child: const Icon(
                          Icons.thumb_up,
                          size: 150,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          },
          separatorBuilder: (context, index) =>
              const SizedBox(height: 20), // Add spacing between cards
        ),
      ),
    );
  }

  List<Widget> displayCards(List<Microwave> microwaves) {
    List<Widget> r = [];
    microwaves.forEach((element) {
      r.add(Padding(
          padding: const EdgeInsets.all(8.0), child: microwaveCard(element)));
    });

    return r;
  }

  Widget microwaveCard(Microwave micro) {
    //bool alreadyLiked = await services.alreadyLiked(micro.id, user.email);
    return GestureDetector(
        onTap: () {
          services.addLike(micro.id, user.email);
        },
        child: Card(
          color: AppColors.loginText.withOpacity(0.5),
          elevation: 2.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadiusInt),
          ),
          child: SizedBox(
            width: 400,
            height: 200,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Material(
                      color: Colors.grey,
                      elevation: 1,
                      shadowColor: AppColors.primaryText.withOpacity(0.4),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(borderRadiusInt),
                        topRight: Radius.circular(borderRadiusInt),
                        bottomLeft: Radius.circular(borderRadiusInt),
                        bottomRight: Radius.circular(borderRadiusInt),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(borderRadiusInt),
                          topRight: Radius.circular(borderRadiusInt),
                          bottomLeft: Radius.circular(borderRadiusInt),
                          bottomRight: Radius.circular(borderRadiusInt),
                        ),
                        child: Image.network(micro.image,
                            width: 140, height: 160, fit: BoxFit.fitHeight),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0, top: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('Edificio: '),
                        Text('Cantidad: ' + micro.quantity.toString()),
                        Text('Edificio: ' + micro.edifice),
                        const Text('Horario Disponible: '),
                        Text('    ' + micro.empy_schedule),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
