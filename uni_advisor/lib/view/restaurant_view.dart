import 'package:flutter/material.dart';
import 'package:uni_advisor/model/models/restaurant.dart';
import 'package:uni_advisor/services/restaurant_services.dart';

import '../configs/colors.dart';

class RestaurantView extends StatefulWidget {
  final Restaurant r;
  RestaurantView({required this.r});
  _RestaurantViewState createState() => _RestaurantViewState();
}

class _RestaurantViewState extends State<RestaurantView> {
  final rService = RestaurantServices();
  String distance = " km";

  @override
  void initState() {
    calculateDistance();
    super.initState();
  }

  void calculateDistance() async {
    String _distance = await rService.calculateDistance(widget.r);
    setState(() {
      distance = "${_distance} Km";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.negativeSpace,
        appBar: AppBar(
          backgroundColor: AppColors.negativeSpace,
          elevation: 0,
          iconTheme: IconThemeData(color: AppColors.primaryText),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: 300.0,
                height: 400.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(
                      15), // Radio de borde de 100 para hacerlo circular
                  child: Image.network(
                    widget.r.image == ""
                        ? 'https://via.placeholder.com/200x200'
                        : widget.r.image, // URL de la imagen
                    fit: BoxFit
                        .cover, // Ajustar la imagen al tamaño del contenedor
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text(
                  widget.r.name,
                  style: TextStyle(
                      color: AppColors.primaryText,
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, bottom: 10, left: 50),
                    child: Text(
                      distance,
                      style:
                          TextStyle(color: AppColors.primaryText, fontSize: 20),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.gps_fixed,
                      color: AppColors.primaryText,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(widget.r.address,
                        style: TextStyle(
                          color: AppColors.primaryText,
                        ))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.type_specimen,
                      color: AppColors.primaryText,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(widget.r.type,
                        style: TextStyle(
                          color: AppColors.primaryText,
                        ))
                  ],
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }

  Widget floatingButton() {
    return Container(
      height: 100,
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.all(15),
      child: FloatingActionButton.extended(
        backgroundColor: AppColors.primary,
        label: const Text(
          "Reseñas",
          style: TextStyle(fontSize: 18),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('/map');
        },
      ),
    );
  }
}
