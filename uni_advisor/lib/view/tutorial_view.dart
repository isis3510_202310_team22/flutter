import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/model/models/teacher.dart';
import 'package:uni_advisor/model/models/tutorial.dart';
import 'package:uni_advisor/model/models/user.dart';
import 'package:uni_advisor/services/teacher_service.dart';
import 'package:uni_advisor/view/teacher_detail_view.dart';
import 'package:uni_advisor/viewModel/bloc/blocAuthentication/authentication_bloc.dart';
import 'package:uni_advisor/viewModel/bloc/blocTeacher/teacher_bloc.dart';
import 'package:uni_advisor/view/hamburger_menu.dart';
import 'package:uni_advisor/viewModel/teacher_view_model.dart';
import 'package:uni_advisor/viewModel/tutorial_view_model.dart';

class TutorialView extends StatefulWidget {
  const TutorialView({super.key});

  @override
  State<TutorialView> createState() => _TutorialViewState();
}

class _TutorialViewState extends State<TutorialView> {
  static List<Tutorial> main_tutorial_list = [];

  List<Tutorial> display_list = List.from(main_tutorial_list);
  bool isLoading = true;
  List<bool> registered = [];

  late User usuario;
  void findUser() {
    var authState = BlocProvider.of<AuthenticationBloc>(context).state;
    if (authState.status == AuthenticationStatus.authenticated) {
      usuario = authState.user;
    }
  }

  void updateList(String value) {
    setState(() {
      display_list = main_tutorial_list
          .where((element) =>
              element.course.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  void updateRegister(int index) {
    var a = registered[index];
    setState(() {
      registered[index] = !a;
    });
  }

  Future<void> updateListInfo(int index) async {
    var connectivityResult = await Connectivity().checkConnectivity();
    TutorialViewModel tutorialViewModel = TutorialViewModel();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      Tutorial tut = main_tutorial_list[index];
      var a = registered[index];
      if (tut.places > 0) {
        List att = tut.attendants;
        if (!a) {
          att.add(usuario.email);
        } else {
          att.remove(usuario.email);
        }
        setState(() {
          registered[index] = !a;
          main_tutorial_list[index] = Tutorial(
              id: tut.id,
              course: tut.course,
              places: !a ? tut.places - 1 : tut.places + 1,
              room: tut.room,
              attendants: att,
              date: tut.date);
          display_list = main_tutorial_list;
          tutorialViewModel.updateTutorial(main_tutorial_list[index]);
        });
        !a
            ? Fluttertoast.showToast(
                msg: 'Tutoria inscrita.',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.TOP,
                timeInSecForIosWeb: 3,
                backgroundColor: Colors.black87,
                textColor: Colors.white,
              )
            : Fluttertoast.showToast(
                msg: 'Tutoria desvinculada.',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.TOP,
                timeInSecForIosWeb: 3,
                backgroundColor: Colors.black87,
                textColor: Colors.white,
              );
      } else {
        Fluttertoast.showToast(
          msg:
              'No se puede inscribir, limite de cupos sobrepasado. Intente más tarde.',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 5,
          backgroundColor: Colors.black87,
          textColor: Colors.white,
        );
      }
    } else {
      Fluttertoast.showToast(
        msg: 'Sin conexión. Intenta más tarde.',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 5,
        backgroundColor: Colors.black87,
        textColor: Colors.white,
      );
    }
  }

  @override
  void initState() {
    findUser();
    fetchData();
    super.initState();
  }

  Future<void> fetchData() async {
    try {
      TutorialViewModel tutorialViewModel = TutorialViewModel();
      print("dios mio");
      List<Tutorial> list = await tutorialViewModel.getAllTutorials();
      List<bool> regis_list =
          await tutorialViewModel.checkUserRegistration(list, usuario.email);
      // print(list);
      print("no se");
      setState(() {
        main_tutorial_list = list;
        registered = regis_list;
        List<Tutorial> display_list = List.from(main_tutorial_list);
        updateList("");
        isLoading = false;
      });
    } catch (error) {
      // Error de conexión o procesamiento
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(child: HamburgerMenu()),
      backgroundColor: AppColors.negativeSpace,
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        elevation: 0.0,
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: isLoading
            ? CircularProgressIndicator()
            : Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Busca a una Tutoria",
                    style: TextStyle(
                        color: AppColors.primaryText,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextField(
                    onChanged: (value) => updateList(value),
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.primary,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide.none,
                      ),
                      hintText: "Escriba el nombre de su tutoria",
                      prefixIcon: Icon(Icons.search),
                      prefixIconColor: AppColors.accent,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: display_list.length,
                        itemBuilder: (context, index) => ListTile(
                            contentPadding: EdgeInsets.all(8.0),
                            title: Text(
                              display_list[index].course,
                              style: TextStyle(
                                  color: AppColors.primaryText,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0),
                            ),
                            subtitle: Text(
                              '${display_list[index].date}',
                              style: TextStyle(color: AppColors.primary),
                            ),
                            trailing: Text(
                              '${display_list[index].places}',
                              style: TextStyle(color: AppColors.primary),
                            ),
                            leading: registered[index]
                                ? Icon(Icons.check)
                                : Icon(Icons.add),
                            onTap: () {
                              updateListInfo(index);
                            })),
                  )
                ],
              ),
      ),
    );
  }
}
