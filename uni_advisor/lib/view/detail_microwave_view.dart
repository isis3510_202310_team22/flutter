import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:uni_advisor/model/models/microwave.dart';
import 'package:uni_advisor/services/preference_manager.dart';

import '../services/microwave_service.dart';

class DetailMicrowaveView extends StatefulWidget {
  @override
  _DetailMicrowaveView createState() => _DetailMicrowaveView();
}

class _DetailMicrowaveView extends State<DetailMicrowaveView> {
  final List<Map<String, String>> comments = [
    {"usuario1": "Commet1"},
    {"usuario2": "Commet2"},
    // Agrega más comentarios según tus necesidades
  ];
  final services = MicrowaveService();
  var Json = {
    "empy_schedule": " 6:00AM- 3:00PM",
    "image":
        "https://ichef.bbci.co.uk/news/640/cpsprodpb/11440/production/_113502707_gettyimages-184328070.jpg",
    "quantity": 4,
    "Edificio": "Mario Laserna (ML)",
    "HorariosConcurridos": "12:20 PM - 3:00 PM",
    "busy_schedule": "12:20 PM - 3:00 PM",
    "Imagen":
        "https://ichef.bbci.co.uk/news/640/cpsprodpb/11440/production/_113502707_gettyimages-184328070.jpg",
    "likes_number": 4,
    "Piso": 5,
    "HorariosDisponibles": "6:00AM - 3:00PM",
    "descriptive_location": "heladería",
    "reviews": "",
  };
  var micro;
  void microwaveLoad() async {
    Map<String, dynamic>? JsonMicro = await PreferenceManager.getMicrowave();
    micro = Microwave.fromJson(JsonMicro!);
  }

  @override
  void initState() {
    // ignore: non_constant_identifier_names
    microwaveLoad();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            // First Part
            Container(
              height: 300, // Adjust the height as needed
              child: Stack(
                children: [
                  CachedNetworkImage(
                    imageUrl:
                        "https://ichef.bbci.co.uk/news/640/cpsprodpb/11440/production/_113502707_gettyimages-184328070.jpg",
                    placeholder: (context, url) =>
                        const CircularProgressIndicator(),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: double.infinity,
                  ),
                  const Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Microondas 1',
                      style: TextStyle(
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50,
            ),
            // Second Part
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      'Likes: ',
                      style: TextStyle(fontSize: 18),
                    ),
                    Text(
                      '100',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                const Text(
                  'Descripción Basica',
                  style: TextStyle(fontSize: 18),
                ),
              ],
            ),
            SizedBox(
              height: 50,
            ),
            // Third and Fourth Part
            Container(
              width: MediaQuery.of(context).size.width * 0.86,
              height: 350, // Adjust the height as needed
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  Expanded(
                    flex:
                        2, // El ListView.builder ocupa 2/4 partes de la pantalla
                    child: ListView.builder(
                      itemCount: comments.length,
                      itemBuilder: (context, index) {
                        final user = comments[index].keys.first;
                        final comment = comments[index].values.first;
                        return ListTile(
                          title: Text(user),
                          subtitle: Text(comment),
                        );
                      },
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Escriba su comentario',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 16),
                  ElevatedButton(
                    onPressed: () {
                      // Add your send button logic here
                    },
                    child: Text('Enviar'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
