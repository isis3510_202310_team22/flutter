import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/viewModel/bloc/blocAuthentication/authentication_bloc.dart';
import 'package:uni_advisor/viewModel/cubit/cubitConnectivity/connectivity_cubit.dart';

class HamburgerMenu extends StatefulWidget {
  const HamburgerMenu({super.key});

  @override
  _HamburgerMenuState createState() => _HamburgerMenuState();
}

class _HamburgerMenuState extends State<HamburgerMenu> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: AppColors.primary,
          height: 200,
          width: 500,
        ),
        Container(
            padding: const EdgeInsets.only(top: 10),
            child: Column(children: <Widget>[
              ListTile(
                title: const Text("Home"),
                leading: const Icon(Icons.home),
                onTap: () {
                  Navigator.of(context).pushNamed('/home');
                },
              ),
              ListTile(
                  title: const Text("Microondas :)"),
                  leading: const Icon(Icons.microwave),
                  onTap: () {
                    Navigator.of(context).pushNamed('/microwave');
                  }),
              ListTile(
                  title: const Text("Profesores"),
                  leading: const Icon(Icons.school),
                  onTap: () {
                    Navigator.of(context).pushNamed('/teachers');
                  }),
              ListTile(
                  title: const Text("Localizar Restaurantes"),
                  leading: const Icon(Icons.map),
                  onTap: () {
                    Navigator.of(context).pushNamed('/map');
                  }),
              ListTile(
                  title: const Text("Buscar Tutorias"),
                  leading: const Icon(Icons.class_),
                  onTap: () {
                    BlocProvider.of<ConnectivityCubit>(context).state
                            is InternetDisconnected
                        ? Fluttertoast.showToast(
                            msg:
                                'Opción valida solo con conexión, intenta más tarde.',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.TOP,
                            timeInSecForIosWeb: 5,
                            backgroundColor: Colors.black87,
                            textColor: Colors.white,
                          )
                        : Navigator.of(context).pushNamed('/tutorials');
                  }),
              ListTile(
                  title: const Text("Mis Tutorias"),
                  leading: const Icon(Icons.assignment_turned_in),
                  onTap: () {
                    BlocProvider.of<ConnectivityCubit>(context).state
                            is InternetDisconnected
                        ? Fluttertoast.showToast(
                            msg:
                                'Opción valida solo con conexión, intenta más tarde.',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.TOP,
                            timeInSecForIosWeb: 5,
                            backgroundColor: Colors.black87,
                            textColor: Colors.white,
                          )
                        : Navigator.of(context).pushNamed('/mine_tutorials');
                  }),
              ListTile(
                title: const Text("Log Out"),
                leading: const Icon(Icons.logout),
                onTap: () {
                  BlocProvider.of<AuthenticationBloc>(context)
                      .add(SignOutRequested());
                  Navigator.of(context).popUntil((route) => route.isFirst);
                },
              ),
            ]))
      ],
    );
  }
}
