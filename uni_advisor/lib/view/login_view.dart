import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/model/serviceAdapter/scraper.dart';
import 'package:uni_advisor/viewModel/bloc/blocAuthentication/authentication_bloc.dart';
import 'package:uni_advisor/viewModel/cubit/cubitConnectivity/connectivity_cubit.dart';
import 'package:uni_advisor/viewModel/scraping_view_model.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginView createState() => _LoginView();
}

class _LoginView extends State<LoginView> {
  @override
  void initState() {
    super.initState();
  }

  Widget errorMessage = Container();
  RegExp pattern = RegExp(
      r"@[a-z]+\.[a-z]+"); // patrón que coincide con direcciones de correo electrónico básicas

  TextEditingController _userController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _passwordx2Controller = TextEditingController();
  String error = "";

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // var scraper =
    //     Scraper("https://decanaturadeestudiantes.uniandes.edu.co/eventtia");
    // var titles = scraper.getTitles();

    // titles.then((valor) {
    //   print("El valor del Titles es: $valor");
    // });

    // var img = scraper.getImages();
    // img.then((valor) {
    //   print("El valor del Images es: $valor");
    // });

    // var dates = scraper.getTexts();
    // dates.then((valor) {
    //   print("El valor del Images es: $valor");
    // });
    // var redirects = scraper.getRedirects();
    // redirects.then((valor) {
    //   print("El valor del Redirects es: $valor");
    // });
    // var svm = ScrapingViewModel(
    //     "https://decanaturadeestudiantes.uniandes.edu.co/eventtia");
    // var dicts = svm.getElements();
    // dicts.then((valor) {
    //   print(valor.length);
    //   print("El valor del Redirects es: $valor");
    // });

    return login();
  }


  Widget login() {
    return DefaultTabController(
        length: 2,
        child: MultiBlocListener(
          listeners: [
            BlocListener<AuthenticationBloc, AuthenticationState>(
              listener: (context, state) {
                if (state.status == AuthenticationStatus.authenticated) {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed("/home");
                } else if (state.status ==
                    AuthenticationStatus.unauthenticated) {
                  Navigator.pop(context);
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Row(
                        children: [
                          Text(state.message),
                        ],
                      ),
                      duration: Duration(seconds: 2),
                    ),
                  );
                } else if (state.status == AuthenticationStatus.unknow) {
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) {
                      return AlertDialog(
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(), // Agrega un icono de carga
                            SizedBox(
                                width:
                                    20), // Agrega un espacio entre el icono y el texto
                            Text('Cargando...'),
                          ],
                        ),
                      );
                    },
                  );
                }
              },
            ),
            BlocListener<ConnectivityCubit, ConnectivityState>(
              listener: (context, state) {
                if (state is InternetDisconnected) {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Row(
                        children: [
                          Text("Usted no cuenta con conexión a internet"),
                        ],
                      ),
                      duration: Duration(days: 365),
                    ),
                  );
                } else {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                }
              },
            ),
          ],
          child: Scaffold(
            backgroundColor: AppColors.primaryDark,
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(300),
              child: AppBar(
                centerTitle: true,
                backgroundColor: AppColors.primary,
                bottom: TabBar(
                  indicator: UnderlineTabIndicator(
                    borderSide:
                        BorderSide(width: 4.0, color: AppColors.loginText),
                  ),
                  tabs: [
                    Tab(
                      text: "SIGN IN",
                    ),
                    Tab(
                      text: "LOG IN",
                    ),
                  ],
                ),
                flexibleSpace: Stack(
                  alignment: Alignment.center,
                  children: [
                    const SizedBox(
                      width: double.infinity,
                      height: double.infinity,
                    ),
                    Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColors.loginText,
                      ),
                      child: const CircleAvatar(
                          radius: 20,
                          backgroundImage:
                              AssetImage("assets/images/uniadvisor_logo.png")),
                    ),
                  ],
                ),
              ),
            ),
            body: TabBarView(
              children: <Widget>[
                // if (connection) Text("paila"),
                // Container para el inicio de sesión
                SingleChildScrollView(
                  child: Container(
                    padding: const EdgeInsets.all(45.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Inserta aquí los widgets necesarios para el formulario de inicio de sesión
                        TextField(
                          maxLength: 40,
                          controller: _userController,
                          style: TextStyle(color: AppColors.loginText),
                          decoration: InputDecoration(
                            labelText: 'Usuario',
                            labelStyle: TextStyle(
                                color: AppColors
                                    .loginText), // Cambiar el color del texto de la etiqueta a blanco
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors
                                        .loginText)), // Cambiar el color del borde a blanco
                            prefixIcon: Icon(
                              Icons.person_outline,
                              color: AppColors.loginText,
                            ),
                          ),
                        ),
                        TextField(
                          controller: _emailController,
                          maxLength: 40,
                          style: TextStyle(color: AppColors.loginText),
                          decoration: InputDecoration(
                            labelText: 'Correo',
                            labelStyle: TextStyle(
                                color: AppColors
                                    .loginText), // Cambiar el color del texto de la etiqueta a blanco
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors
                                        .loginText)), // Cambiar el color del borde a blanco
                            prefixIcon: Icon(
                              Icons.mail_outline,
                              color: AppColors.loginText,
                            ),
                          ),
                        ),
                        TextField(
                          style: TextStyle(color: AppColors.loginText),
                          controller: _passwordController,
                          maxLength: 40,
                          obscureText: true,
                          decoration: InputDecoration(
                            labelText: 'Contraseña',
                            labelStyle: TextStyle(
                                color: AppColors
                                    .loginText), // Cambiar el color del texto de la etiqueta a blanco
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors
                                        .loginText)), // Cambiar el color del borde a blanco
                            prefixIcon: Icon(
                              Icons.key,
                              color: AppColors.loginText,
                            ),
                          ),
                        ),
                        TextField(
                          controller: _passwordx2Controller,
                          maxLength: 40,
                          style: TextStyle(color: AppColors.loginText),
                          obscureText: true,
                          decoration: InputDecoration(
                            labelText: 'Confirmación de Contraseña',

                            labelStyle: TextStyle(
                                color: AppColors
                                    .loginText), // Cambiar el color del texto de la etiqueta a blanco
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors
                                        .loginText)), // Cambiar el color del borde a blanco
                            prefixIcon: Icon(
                              Icons.done_all,
                              color: AppColors.loginText,
                            ),
                          ),
                        ),
                        const SizedBox(
                            height:
                                16.0), // Espacio de 16 puntos entre los campos de texto y los botones
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppColors.primary),
                          ),
                          onPressed: a()
                              ? null
                              : () => {
                                    BlocProvider.of<AuthenticationBloc>(context)
                                        .add(SignUpRequested(
                                            _emailController.text,
                                            _passwordController.text,
                                            _userController.text,
                                            _passwordx2Controller.text))
                                  },
                          child: const Text('Confirmar'),
                        ),
                      ],
                    ),
                  ),
                ),
                // Container para el registro
                SingleChildScrollView(
                  child: Container(
                    padding: const EdgeInsets.all(45.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Inserta aquí los widgets necesarios para el formulario de registro
                        TextField(
                          maxLength: 40,
                          controller: _emailController,
                          style: TextStyle(color: AppColors.loginText),
                          decoration: InputDecoration(
                            labelText: 'Correo Electrónico',
                            labelStyle: TextStyle(
                                color: AppColors
                                    .loginText), // Cambiar el color del texto de la etiqueta a blanco
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors
                                        .loginText)), // Cambiar el color del borde a blanco
                            prefixIcon: Icon(
                              Icons.person_outline,
                              color: AppColors.loginText,
                            ),
                          ),
                        ),
                        TextField(
                          maxLength: 40,
                          controller: _passwordController,
                          style: TextStyle(color: AppColors.loginText),
                          obscureText: true,
                          decoration: InputDecoration(
                            labelText: 'Contraseña',
                            labelStyle: TextStyle(
                              color: AppColors.loginText,
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: AppColors.loginText),
                            ),
                            prefixIcon: Icon(
                              Icons.key,
                              color: AppColors.loginText,
                            ),
                          ),
                        ),
                        const SizedBox(
                            height:
                                16.0), // Espacio de 16 puntos entre los campos de texto y los botones
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                AppColors.primary),
                          ),
                          onPressed: () {
                            var a = BlocProvider.of<ConnectivityCubit>(context);
                            var b = a.state;
                            if (b is InternetConnected) {
                              BlocProvider.of<AuthenticationBloc>(context).add(
                                  LoginRequested(_emailController.text,
                                      _passwordController.text));
                            } else {
                              Fluttertoast.showToast(
                                msg: 'Sin conexión. Intenta más tarde.',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.TOP,
                                timeInSecForIosWeb: 5,
                                backgroundColor: Colors.black87,
                                textColor: Colors.white,
                              );
                            }
                          },
                          child: const Text('ENTRAR'),
                        ),
                        errorMessage,
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  bool a() {
    var connectivityResult = Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}
