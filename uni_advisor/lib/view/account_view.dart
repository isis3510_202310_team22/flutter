import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/viewModel/account_view_model.dart';
import 'package:uni_advisor/viewModel/cubit/cubitConnectivity/connectivity_cubit.dart';

class AccountView extends StatefulWidget {
  const AccountView({super.key});

  @override
  State<AccountView> createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  final double profileHeight = 180;
  final double top = 50;

  bool isButtonVisible = false;
  late AccountViewModel? accountViewModel;

  TextEditingController emailController = TextEditingController();
  TextEditingController telController = TextEditingController();
  TextEditingController uniController = TextEditingController();
  TextEditingController carController = TextEditingController();
  TextEditingController semController = TextEditingController();

  String? _telErrorMessage = null;
  String? _uniErrorMessage = null;
  String? _carErrorMessage = null;
  String? _semErrorMessage = null;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    accountViewModel = context.watch<AccountViewModel>();
    return BlocListener<ConnectivityCubit, ConnectivityState>(
      listener: (context, state) {
        if (state is InternetDisconnected) {
          if (accountViewModel!.imageLoading) {
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  children: [
                    Flexible(
                      child: Text(
                        'No hay conexión a internet, cuando retome la conexión su foto será guardada',
                        overflow:
                            TextOverflow.clip, // Customize overflow behavior
                        softWrap:
                            true, // Allow text to wrap within the available space
                      ),
                    ),
                  ],
                ),
                duration: Duration(seconds: 10),
              ),
            );
          }
          accountViewModel!.setConnected(false);
        } else if (state is InternetConnected) {
          accountViewModel!.setConnected(true);
        }
      },
      child: Scaffold(
        appBar: AppBar(
            title: Text(
              'Mi Perfil',
              style: TextStyle(color: AppColors.secondaryText),
            ),
            automaticallyImplyLeading: true,
            backgroundColor: AppColors.primary),
        body: _ui(),
        bottomNavigationBar: Container(
          height: 40,
          color: AppColors.primary,
        ),
      ),
    );
  }

  @override
  void dispose() {
    accountViewModel?.setFillFields(true);
    super.dispose();
  }

  _ui() {
    if (accountViewModel!.loading) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            const SizedBox(height: 16.0),
          ],
        ),
      );
    }
    return buildAccountBody();
  }

  _image() {
    if (accountViewModel!.imageLoading) {
      return FileImage(accountViewModel!.image);
    }
    return CachedNetworkImageProvider(accountViewModel?.user.ppUrl == ''
        ? 'https://firebasestorage.googleapis.com/v0/b/moviles2023-c0911.appspot.com/o/pps%2Fdepositphotos_137014128-stock-illustration-user-profile-icon.jpg?alt=media&token=29948f11-2760-4d41-a0d3-a85e108d85f9'
        : accountViewModel!.user.ppUrl);
  }

  _checkSaveButtonVisibility() {
    if (uniController.text != accountViewModel!.user.university ||
        telController.text != accountViewModel!.user.phoneNumber ||
        carController.text != accountViewModel!.user.career ||
        semController.text != accountViewModel!.user.semester) {
      if (_phoneNumberIsValid() &&
          _universityIsValid() &&
          _carIsValid() &&
          _semIsValid()) {
        setState(() {
          isButtonVisible = true;
        });
      } else {
        setState(() {
          isButtonVisible = false;
        });
      }
    }
  }

  _phoneNumberIsValid() {
    RegExp regex = RegExp(r'^\+[1-9]{1,2}[0-9]{10}$');
    if (telController.text.isEmpty) {
      setState(() {
        _telErrorMessage = 'Este campo es obligatorio.';
      });
      return false;
    } else if (!regex.hasMatch(telController.text)) {
      setState(() {
        _telErrorMessage = 'Formato esperado +XXXXXXXXXXXX';
      });
      return false;
    } else {
      setState(() {
        _telErrorMessage = null;
      });
      return true;
    }
  }

  _carIsValid() {
    if (carController.text.isEmpty) {
      setState(() {
        _carErrorMessage = 'Este campo es obligatorio.';
      });
      return false;
    } else {
      setState(() {
        _carErrorMessage = null;
      });
      return true;
    }
  }

  _universityIsValid() {
    if (uniController.text.isEmpty) {
      setState(() {
        _uniErrorMessage = 'Este campo es obligatorio.';
      });
      return false;
    } else {
      setState(() {
        _uniErrorMessage = null;
      });
      return true;
    }
  }

  _semIsValid() {
    RegExp regex = RegExp(r'^(?:[1-9]|0[1-9]|1[0-9]|20)$');
    if (semController.text.isEmpty) {
      setState(() {
        _semErrorMessage = 'Este campo es obligatorio';
      });
      return false;
    } else if (!regex.hasMatch(semController.text)) {
      setState(() {
        _semErrorMessage = 'Número entre 1 y 20';
      });
      return false;
    } else {
      setState(() {
        _semErrorMessage = null;
      });
      return true;
    }
  }

  Widget buildAccountBody() => Container(
      color: AppColors.negativeSpace,
      height: double.infinity,
      width: double.infinity,
      child: SingleChildScrollView(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: buildProfilePicture(),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 15),
            child: buildUsername(),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 40, right: 40, top: 15, bottom: 20),
            child: buildUserInfo(),
          )
        ]),
      ));

  Widget buildProfilePicture() => Stack(
        children: [
          CircleAvatar(
              radius: profileHeight / 2,
              backgroundColor: Colors.grey.shade800,
              child: Container(
                  decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(fit: BoxFit.cover, image: _image()),
              ))),
          Positioned(
              child: InkWell(
            onTap: () {
              showModalBottomSheet(
                  context: context, builder: ((builder) => bottomSheet()));
            },
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 3, color: AppColors.primary),
                  color: AppColors.negativeSpace),
              child: Icon(
                Icons.edit,
                color: AppColors.primary,
                size: 28,
              ),
            ),
          ))
        ],
      );

  Widget buildUsername() => Text(
        accountViewModel!.user.name,
        style: TextStyle(
            fontFamily: 'Arial',
            fontSize: 25,
            color: AppColors.primaryText,
            fontWeight: FontWeight.bold),
      );

  Widget buildUserInfo() {
    if (accountViewModel!.fillFields) {
      emailController.text = accountViewModel!.user.email;
      telController.text = accountViewModel!.user.phoneNumber;
      uniController.text = accountViewModel!.user.university;
      carController.text = accountViewModel!.user.career;
      semController.text = accountViewModel!.user.semester;
      accountViewModel?.setFillFields(false);
    }
    return Container(
        //height: 400,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: AppColors.secondary,
        ),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            TextField(
              controller: emailController,
              readOnly: true,
              style: TextStyle(
                color: AppColors.primaryText,
              ),
              decoration: InputDecoration(
                labelText: 'Email',
                labelStyle: TextStyle(
                  color: AppColors.primaryText,
                ),
                hintStyle: TextStyle(
                  color: AppColors.primaryText,
                ),
              ),
            ),
            TextField(
              controller: telController,
              onChanged: (value) {
                _checkSaveButtonVisibility();
              },
              style: TextStyle(
                color: AppColors.primaryText,
              ),
              decoration: InputDecoration(
                  labelText: 'Número de telefono',
                  labelStyle: TextStyle(
                    color: AppColors.primaryText,
                  ),
                  errorText: _telErrorMessage),
            ),
            TextField(
              controller: uniController,
              onChanged: (value) => {_checkSaveButtonVisibility()},
              style: TextStyle(
                color: AppColors.primaryText,
              ),
              decoration: InputDecoration(
                labelText: 'Universidad',
                labelStyle: TextStyle(
                  color: AppColors.primaryText,
                ),
                hintStyle: TextStyle(
                  color: AppColors.primaryText,
                ),
                errorText: _uniErrorMessage,
              ),
            ),
            TextField(
              controller: carController,
              onChanged: (value) => {_checkSaveButtonVisibility()},
              style: TextStyle(
                color: AppColors.primaryText,
              ),
              decoration: InputDecoration(
                  labelText: 'Carrera',
                  labelStyle: TextStyle(
                    color: AppColors.primaryText,
                  ),
                  hintStyle: TextStyle(
                    color: AppColors.primaryText,
                  ),
                  errorText: _carErrorMessage),
            ),
            TextField(
              controller: semController,
              onChanged: (value) => {_checkSaveButtonVisibility()},
              style: TextStyle(
                color: AppColors.primaryText,
              ),
              decoration: InputDecoration(
                  labelText: 'Semestre',
                  labelStyle: TextStyle(
                    color: AppColors.primaryText,
                  ),
                  hintStyle: TextStyle(
                    color: AppColors.primaryText,
                  ),
                  errorText: _semErrorMessage),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Visibility(
                  visible: (isButtonVisible),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all<Color>(AppColors.primary),
                        foregroundColor: MaterialStateProperty.all<Color>(
                            AppColors.secondary)),
                    onPressed: () async {
                      await accountViewModel!.saveUser(
                          telController.text,
                          uniController.text,
                          carController.text,
                          semController.text);
                      // ignore: use_build_context_synchronously
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text('Información guardada'),
                            content: const Text(
                                'La información ha sido guardada correctamente.'),
                            actions: [
                              TextButton(
                                child: const Text('OK'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Text(
                      'Guardar',
                      style: TextStyle(color: AppColors.secondaryText),
                    ),
                  ),
                ),
              ),
            ),
          ]),
        ));
  }

  Widget bottomSheet() {
    return Container(
      height: 100.0,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          const Text(
            "Escoge una foto de perfil",
            style: TextStyle(
              fontSize: 20.0,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            TextButton.icon(
              icon: const Icon(Icons.camera),
              onPressed: () {
                accountViewModel!.selectImage(ImageSource.camera).then((value) {
                  if (!accountViewModel!.connected) {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Row(
                          children: [
                            Flexible(
                              child: Text(
                                'No hay conexión a internet, cuando retome la conexión su foto será guardada',
                                overflow: TextOverflow
                                    .clip, // Customize overflow behavior
                                softWrap:
                                    true, // Allow text to wrap within the available space
                              ),
                            ),
                          ],
                        ),
                        duration: Duration(seconds: 10),
                      ),
                    );
                  }
                });
              },
              style: ButtonStyle(
                foregroundColor:
                    MaterialStateProperty.all<Color>(AppColors.primary),
              ),
              label: const Text("Camera"),
            ),
            TextButton.icon(
              icon: const Icon(Icons.image),
              onPressed: () {
                accountViewModel!.selectImage(ImageSource.camera).then((value) {
                  if (!accountViewModel!.connected) {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Row(
                          children: [
                            Flexible(
                              child: Text(
                                'No hay conexión a internet, cuando retome la conexión su foto será guardada',
                                overflow: TextOverflow
                                    .clip, // Customize overflow behavior
                                softWrap:
                                    true, // Allow text to wrap within the available space
                              ),
                            ),
                          ],
                        ),
                        duration: Duration(seconds: 10),
                      ),
                    );
                  }
                });
              },
              style: ButtonStyle(
                foregroundColor:
                    MaterialStateProperty.all<Color>(AppColors.primary),
              ),
              label: const Text("Gallery"),
            ),
          ])
        ],
      ),
    );
  }
}
