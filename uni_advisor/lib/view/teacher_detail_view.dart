import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/model/models/comment.dart';
import 'package:uni_advisor/model/models/restaurant.dart';
import 'package:uni_advisor/model/models/teacher.dart';
import 'package:uni_advisor/model/models/user.dart';
import 'package:uni_advisor/services/teacher_service.dart';
import 'package:uni_advisor/view/login_view.dart';
import 'package:uni_advisor/view/restaurant_view.dart';
import 'package:uni_advisor/viewModel/bloc/blocAuthentication/authentication_bloc.dart';
import 'package:uni_advisor/viewModel/bloc/blocTeacher/teacher_bloc.dart';
import 'package:uni_advisor/view/hamburger_menu.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:uni_advisor/viewModel/comment_view_model.dart';

class TeacherDetailView extends StatefulWidget {
  final Teacher teacher;
  TeacherDetailView({required this.teacher});
  State<TeacherDetailView> createState() => _TeacherDetailViewState();
}

class _TeacherDetailViewState extends State<TeacherDetailView> {
  List<Comment> comments = [];
  bool isLoading = true;
  TextEditingController commentController = TextEditingController();
  double rating = 3.0;

  late User usuario;
  void findUser() {
    var authState = BlocProvider.of<AuthenticationBloc>(context).state;
    if (authState.status == AuthenticationStatus.authenticated) {
      usuario = authState.user;
    }
  }

  Future<void> fetchData() async {
    try {
      CommentViewModel commentViewModel = CommentViewModel();
      List<Comment> list = await commentViewModel.getAllComments();
      list = await commentViewModel.filterComment(list, widget.teacher.email);
      setState(() {
        comments = list;
        isLoading = false;
      });
    } catch (error) {
      // Error de conexión o procesamiento
      setState(() {
        isLoading = false;
      });
    }
  }

  void addComment() {
    setState(() {
      final newComment = Comment(
          id: usuario.email + "_" + widget.teacher.id,
          idTeacher: widget.teacher.email,
          idUser: usuario.email,
          text: commentController.text,
          rating: rating);
      CommentViewModel commentViewModel = CommentViewModel();
      commentViewModel.saveComment(newComment);
      comments.add(newComment);
      commentController.clear();
      rating = 3.0;
    });
    Navigator.pop(
        context); // Cerrar el formulario flotante después de agregar un comentario
  }

  void openAddCommentDialog() {
    userCommented(usuario.email)
        ? showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Comentario Rechazado',
                    style: TextStyle(color: AppColors.primaryText)),
                backgroundColor: AppColors.negativeSpace,
                content: SingleChildScrollView(
                    child: Text(
                  "Usted ya registro un comentario sobre este profesor, solo lo puede hacer una vez.",
                  style: TextStyle(color: AppColors.primaryText),
                )),
                actions: <Widget>[
                  TextButton(
                    child: Text(
                      'Entendido',
                      style: TextStyle(color: AppColors.primary),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            })
        : showDialog(
            context: context,
            builder: (BuildContext context) {
              return Container(
                width: 30,
                height: 40,
                child: AlertDialog(
                  backgroundColor: AppColors.negativeSpace,
                  title: Text('Agregar comentario',
                      style: TextStyle(color: AppColors.primaryText)),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      RatingBar.builder(
                        initialRating: 3,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {
                          this.rating = rating;
                        },
                      ),
                      SizedBox(
                        height: 40,
                        width: 80,
                      ),
                      Container(
                        width: 240,
                        child: TextFormField(
                          maxLength: 120,
                          controller: commentController,
                          maxLines: 4, // Permite múltiples líneas
                          decoration: InputDecoration(
                            labelText: 'Comentario:',
                          ),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(AppColors
                            .primary), // Cambia el color de fondo del botón a rojo
                      ),
                      child: Text(
                        'Cancelar',
                        style: TextStyle(color: AppColors.primaryText),
                      ),
                      onPressed: () {
                        Navigator.pop(
                            context); // Cerrar el formulario flotante sin agregar un comentario
                      },
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(AppColors
                            .primary), // Cambia el color de fondo del botón a rojo
                      ),
                      child: Text('Enviar',
                          style: TextStyle(color: AppColors.primaryText)),
                      onPressed: addComment,
                    ),
                  ],
                ),
              );
            },
          );
  }

  bool userCommented(String userEmail) {
    var res = false;
    for (var comment in comments) {
      if (comment.idUser == userEmail) {
        res = true;
      }
    }
    return res;
  }

  @override
  void initState() {
    findUser();
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.negativeSpace,
      appBar: AppBar(
        title: Text('Perfil de Profesor'),
        backgroundColor: AppColors.primary,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Card(
              color: AppColors.negativeSpace,
              child: isLoading
                  ? CircularProgressIndicator()
                  : Column(
                      children: [
                        Container(
                          color: AppColors.negativeSpace,
                          child: Container(
                            width: 200,
                            child: CachedNetworkImage(
                              imageUrl: widget.teacher.urlImage,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(), // Opcional: muestra un indicador de carga mientras se carga la imagen
                              errorWidget: (context, url, error) => Icon(Icons
                                  .error), // Opcional: muestra un ícono de error si no se puede cargar la imagen
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            widget.teacher.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22,
                                color: AppColors.primaryText),
                          ),
                          subtitle: Text(
                            widget.teacher.mainCourse,
                            style: TextStyle(
                              color: AppColors.primary,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
            ),
            ListTile(
                title: Text(
              "Comentarios",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                  color: AppColors.primaryText),
            )),
            Expanded(
              child: ListView.builder(
                itemCount: comments.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    color: AppColors.negativeSpace,
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Text(comments[index].idUser,
                              style: TextStyle(
                                  color: AppColors.primaryText, fontSize: 18)),
                          subtitle: RatingBarIndicator(
                            rating: comments[index].rating,
                            itemBuilder: (context, index) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            itemCount: 5,
                            itemSize: 20.0,
                            direction: Axis.horizontal,
                          ),
                        ),
                        ListTile(
                          title: Text(comments[index].text,
                              style: TextStyle(color: AppColors.primaryText)),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.primary,
        child: Icon(
          Icons.add,
          color: AppColors.negativeSpace,
        ),
        onPressed: openAddCommentDialog,
      ),
    );
  }
}
