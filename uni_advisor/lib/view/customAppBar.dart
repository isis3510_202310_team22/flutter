import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final VoidCallback onPressed;
  final bool isMenu;
  final String title;

  CustomAppBar({required this.onPressed, this.isMenu = true, required this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      leading: isMenu
          ? IconButton(
              icon: Icon(Icons.menu, color: Colors.black),
              onPressed: onPressed,
            )
          : IconButton(
              icon: Icon(Icons.arrow_back_ios_rounded, color: Colors.black),
              onPressed: onPressed,
            ),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text(
            'Tienes',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
          ),
          Text(
            'ganas de comer',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ],
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.favorite_border, color: Colors.black),
          onPressed: () {},
        ),
        SizedBox(width: 16),
        GestureDetector(
          onTap: () {},
          child: ClipOval(
            child: Image.asset(
              '../assets/images/profile.png',
              height: 36,
              width: 36,
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(width: 16),
      ],
    );
  }
  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);


}
