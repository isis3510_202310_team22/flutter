import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/model/models/teacher.dart';
import 'package:uni_advisor/services/teacher_service.dart';
import 'package:uni_advisor/view/teacher_detail_view.dart';
import 'package:uni_advisor/viewModel/bloc/blocTeacher/teacher_bloc.dart';
import 'package:uni_advisor/view/hamburger_menu.dart';
import 'package:uni_advisor/viewModel/teacher_view_model.dart';

class TeacherView extends StatefulWidget {
  const TeacherView({super.key});

  @override
  State<TeacherView> createState() => _TeacherViewState();
}

class _TeacherViewState extends State<TeacherView> {
  static List<Teacher> main_teachers_list = [];

  List<Teacher> display_list = List.from(main_teachers_list);
  bool isLoading = true;

  void updateList(String value) {
    setState(() {
      display_list = main_teachers_list
          .where((element) =>
              element.name.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  @override
  void initState() {
    BlocProvider.of<TeacherBloc>(context).add(RequestAllTeachers());
    fetchData();
    super.initState();
  }

  Future<void> fetchData() async {
    try {
      TeacherViewModel teacherViewModel = TeacherViewModel();
      List<Teacher> list = await teacherViewModel.getAllTeachers();
      setState(() {
        main_teachers_list = list;
        List<Teacher> display_list = List.from(main_teachers_list);
        updateList("");
        isLoading = false;
      });
    } catch (error) {
      // Error de conexión o procesamiento
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(child: HamburgerMenu()),
      backgroundColor: AppColors.negativeSpace,
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        elevation: 0.0,
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: isLoading
            ? CircularProgressIndicator()
            : Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Busca a tu Profesor",
                    style: TextStyle(
                        color: AppColors.primaryText,
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextField(
                    onChanged: (value) => updateList(value),
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.primary,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide.none,
                      ),
                      hintText: "Escriba el nombre de su profesor",
                      prefixIcon: Icon(Icons.search),
                      prefixIconColor: AppColors.accent,
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Expanded(
                    child: ListView.builder(
                        itemCount: display_list.length,
                        itemBuilder: (context, index) => ListTile(
                            contentPadding: EdgeInsets.all(8.0),
                            title: Text(
                              display_list[index].name,
                              style: TextStyle(
                                  color: AppColors.primaryText,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0),
                            ),
                            subtitle: Text(
                              '${display_list[index].email}',
                              style: TextStyle(color: AppColors.primary),
                            ),
                            trailing: Text(
                              '${display_list[index].mainCourse}',
                              style: TextStyle(color: AppColors.primary),
                            ),
                            leading: CachedNetworkImage(
                              imageUrl: display_list[index].urlImage,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(), // Widget de carga mientras se descarga la imagen
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => TeacherDetailView(
                                      teacher: display_list[
                                          index]), // Reemplaza NextScreen con la pantalla a la que deseas navegar
                                ),
                              );
                            })),
                  )
                ],
              ),
      ),
    );
  }
}
