import 'dart:async';
import 'dart:ui' as ui;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/configs/constants.dart';
import 'package:uni_advisor/model/models/restaurant.dart';
import 'package:uni_advisor/services/location_service.dart';
import 'package:uni_advisor/services/preference_manager.dart';
import 'package:uni_advisor/services/restaurant_services.dart';
import 'package:uni_advisor/viewModel/bloc/blocMap/map_bloc.dart';
import 'dart:math' as math;

import 'package:uni_advisor/viewModel/cubit/cubitConnectivity/connectivity_cubit.dart';

class MapView extends StatefulWidget {
  const MapView({super.key});

  @override
  State<MapView> createState() => MapViewState();
}

class MapViewState extends State<MapView> {
  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();

  bool isLoading = true;
  //late Position location;
  List<Restaurant> restaurants = [];

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final LocationService locationService = LocationService();
  final RestaurantServices restaurantServices = RestaurantServices();

  Set<Marker> markers = {};

  List<LatLng> polylineCoordinates = [];

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  Map<String, Marker> _markers = {};

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MapBloc()..add(LoadMap()),
      child: Scaffold(
          endDrawer: _restaurantDrawer(),
          appBar: AppBar(
              title: Text(
                'Localizar Restaurantes',
                style: TextStyle(color: AppColors.secondaryText),
              ),
              backgroundColor: AppColors.primary),
          body: BlocBuilder<MapBloc, MapState>(
            builder: (context, state) {
              if (state is MapLoading) {
                return CircularProgressIndicator(color: AppColors.primary);
              }
              if (state is MapLoaded) {
                markers.add(
                  Marker(
                      markerId: const MarkerId('userMarker'),
                      position: LatLng(
                          state.location.latitude!, state.location.longitude!)),
                );
                return Stack(
                  children: [
                    GoogleMap(
                      initialCameraPosition: CameraPosition(
                          target: LatLng(state.location.latitude!,
                              state.location.longitude!),
                          zoom: 17),
                      onMapCreated: (GoogleMapController controller) {
                        _controller.complete(controller);
                      },
                      polylines: {
                        Polyline(
                          polylineId: PolylineId("route"),
                          points: polylineCoordinates,
                          color: AppColors.primary,
                          width: 5,
                        )
                      },
                      markers: markers,
                      zoomControlsEnabled: false,
                    ),
                  ],
                );
              } else {
                return Text('Something went wrong');
              }
            },
          )),
    );
  }

  _restaurantDrawer() {
    return BlocBuilder<MapBloc, MapState>(
      builder: (context, state) {
        if (state is MapLoaded) {
          return Drawer(
            child: ListView.builder(
              itemCount: state.restaurants.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      markers = {
                        Marker(
                            markerId: const MarkerId('userMarker'),
                            position: LatLng(state.location.latitude!,
                                state.location.longitude!)),
                        Marker(
                            markerId: const MarkerId('destinationMarker'),
                            position: LatLng(
                                double.parse(state.restaurants[index].latitud),
                                double.parse(
                                    state.restaurants[index].longitud)),
                            infoWindow: InfoWindow(
                              title: state.restaurants[index].name,
                              snippet: state.restaurants[index].address,
                            ))
                      };
                    });
                    _animateCameraToMarkers();
                    getRoutePoints();
                    Navigator.pop(context);
                  },
                  child: Card(
                    child: Column(
                      children: [
                        AspectRatio(
                            aspectRatio: 16 / 9,
                            child: CachedNetworkImage(
                                imageUrl: state.restaurants[index].image,
                                fit: BoxFit.cover)),
                        ListTile(
                          title: Text(state.restaurants[index].name),
                          subtitle: Text(state.restaurants[index].address),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          );
        }
        if (state is MapLoading) {
          return CircularProgressIndicator(color: AppColors.primary);
        } else {
          return Text('Something went wrong');
        }
      },
    );
  }

  void _animateCameraToMarkers() async {
    if (markers.length < 2) return;

    double minLat = double.infinity;
    double maxLat = -double.infinity;
    double minLng = double.infinity;
    double maxLng = -double.infinity;

    // Find the minimum and maximum latitude and longitude among the markers
    for (Marker marker in markers) {
      final lat = marker.position.latitude;
      final lng = marker.position.longitude;
      minLat = math.min(minLat, lat);
      maxLat = math.max(maxLat, lat);
      minLng = math.min(minLng, lng);
      maxLng = math.max(maxLng, lng);
    }

    // Calculate the center of the bounding box and the zoom level
    final centerLat = (minLat + maxLat) / 2;
    final centerLng = (minLng + maxLng) / 2;
    final center = LatLng(centerLat, centerLng);
    final bounds = LatLngBounds(
        southwest: LatLng(minLat, minLng), northeast: LatLng(maxLat, maxLng));

    _controller.future.then((value) => {
          value
              .animateCamera(CameraUpdate.newLatLngBounds(bounds, 50))
              .then((_) {})
        });
  }

  void getRoutePoints() async {
    var connectivityState = BlocProvider.of<ConnectivityCubit>(context);
    polylineCoordinates.clear();
    print('maptest____ ${connectivityState.state}');

    List<PointLatLng> stops = [
      for (var marker in markers)
        PointLatLng(marker.position.latitude, marker.position.longitude)
    ];

    PreferenceManager.getPolylinePoints(stops[0].latitude, stops[0].longitude,
            stops[1].latitude, stops[1].longitude)
        .then((points) => {
              if (points.isNotEmpty)
                {
                  points.forEach((PointLatLng point) {
                    polylineCoordinates
                        .add(LatLng(point.latitude, point.longitude));
                  }),
                  if (connectivityState.state is InternetDisconnected){
                    Fluttertoast.showToast(
                                msg: 'Esta ruta puede estar desactualizada por que no hay conexión.',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.TOP,
                                timeInSecForIosWeb: 5,
                                backgroundColor: Colors.black87,
                                textColor: Colors.white,
                              )
                  },
                setState(() {})
                } else {
                  Fluttertoast.showToast(
                                msg: 'No pudimos encontrar una ruta, revisa tu conexión',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.TOP,
                                timeInSecForIosWeb: 5,
                                backgroundColor: Colors.black87,
                                textColor: Colors.white,
                              )
                },
            })
        .catchError((e) => print("Hubo un error: ${e}"));

    
  }
}
