import 'dart:core';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/model/models/user.dart';
import 'package:uni_advisor/services/restaurant_services.dart';
import 'package:uni_advisor/view/hamburger_menu.dart';
import 'package:uni_advisor/viewModel/bloc/blocAuthentication/authentication_bloc.dart';
import 'package:uni_advisor/viewModel/account_view_model.dart';
import 'package:uni_advisor/view/restaurant_view.dart';
import 'package:uni_advisor/viewModel/bloc/blocRestaurants/restaurant_bloc.dart';
import 'package:uni_advisor/viewModel/cubit/cubitConnectivity/connectivity_cubit.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  late PageController _pageController;
  List<Widget> cards = [];
  Widget floatingB = Container();

  var isConnected;
  //Encontrar Usuario
  late User usuario;
  void findUser() {
    var authState = BlocProvider.of<AuthenticationBloc>(context).state;
    print("sera");
    if (authState.status == AuthenticationStatus.authenticated) {
      print("sisisiisi");
      usuario = authState.user;
    }
  }

  @override
  void initState() {
    findUser();
    int _currentPage = 0;
    super.initState();
    BlocProvider.of<RestaurantBloc>(context).add(RequestAllRestaurants());
    isConnected = BlocProvider.of<ConnectivityCubit>(context).state;
    _pageController =
        PageController(initialPage: _currentPage, viewportFraction: 0.8);
  }

  basicView() {
    return Container(
        color: AppColors.negativeSpace,
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          children: [
            searchField(_searchController),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 45),
                          child: Text(
                            "Exprorar restaurantes",
                            style: TextStyle(
                                color: AppColors.primaryText,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    _horizontalMenu(),
                    Column(children: cards),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  Widget getAllRestaurants() {
    return (Container(
        padding: const EdgeInsets.only(top: 15),
        child: Column(
          children: [
            searchField(_searchController),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 45),
                          child: Text(
                            "Explorar restaurantes",
                            style: TextStyle(
                                color: AppColors.primaryText,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    _horizontalMenu(),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      _cardPlace("image", "rName", "distance", 10, ""),
                    ]),
                  ],
                ),
              ),
            ),
          ],
        )));
  }

//Lista que se utiliza para definir las opcciones de la horizontal bar
  List<String> opciones = ['Todos', 'Eventos', 'Cercanos', 'Mas Vistos'];
//Identifiacdor para cambiar estado en la horizantal bar
  int _currentIndex = 0;
  final TextEditingController _searchController = TextEditingController();
  final services = RestaurantServices();
  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      drawer: const Drawer(
        child: HamburgerMenu(),
      ),
      appBar: _custoAppBar(),
      body: BlocBuilder<RestaurantBloc, RestaurantState>(
        builder: ((context, state) {
          if (state is RestaurantsLoading) {
            cards = [
              CircularProgressIndicator(), // Agrega un icono de carga
              SizedBox(
                  width: 20), // Agrega un espacio entre el icono y el texto
              const Text('Cargando... Espere un Momento'),
            ];
          } else {
            floatingB = floatingButton();
            if (state is AllRestaurantsObtained && _currentIndex == 0) {
              cards = displayCards(state.restaurants);
            }
            if (state is PopularRestaurantsObtained && _currentIndex == 3) {
              cards = displayCards(state.restaurants);
            }
            if (state is DistancedRestaurantsObtained && _currentIndex == 2) {
              cards = displayCards(state.restaurants);
            }
            if (state is EventsObtained) {
              cards = [
                Container(
                  height: 350,
                  child: PageView.builder(
                      itemCount: state.events.length,
                      physics: const ClampingScrollPhysics(),
                      controller: _pageController,
                      itemBuilder: (context, index) {
                        return carouselView(index, state.events);
                      }),
                )
              ];
              var internet = BlocProvider.of<ConnectivityCubit>(context).state;
              if (internet is InternetDisconnected) {
                cards.add(
                  Column(
                    children: [
                      //// Agrega un espacio entre el icono y el texto
                      SizedBox(
                        height: 25,
                      ),
                      Container(
                        color: AppColors
                            .secondary, // Color de fondo del contenedor
                        child: Text(
                          'Esta información es antigua, cuando tenga internet recargue.', // Texto que se muestra dentro del contenedor
                          style: TextStyle(
                            color: AppColors.primaryText, // Color del texto
                            fontSize: 18, // Tamaño del texto
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                );
              }
            }
          }

          return basicView();
        }),
      ),
      // floatingActionButton: floatingB,
    ));
  }

  Widget carouselView(int index, list) {
    return Container(
      width: 5,
      child: AnimatedBuilder(
        animation: _pageController,
        builder: (context, child) {
          double value = 0.0;
          if (_pageController.position.haveDimensions) {
            value = index.toDouble() - (_pageController.page ?? 0);
            value = (value * 0.038).clamp(-1, 1);
            print("legue hasta aquiiiiii");
          }
          return Transform.rotate(
            angle: 3.14 * value,
            child: carouselCard(list[index]),
          );
        },
      ),
    );
  }

  Widget carouselCard(Map<String, String> data) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Hero(
              tag: data["Title"]!,
              child: GestureDetector(
                onTap: () => launchUrl(Uri.parse(data["Redirect"]!)),
                child: Container(
                  decoration: BoxDecoration(
                      color: AppColors.primary,
                      borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(
                          image: CachedNetworkImageProvider(data["Image"]!),
                          fit: BoxFit.fill),
                      boxShadow: const [
                        BoxShadow(
                            offset: Offset(0, 4),
                            blurRadius: 2,
                            color: Colors.black26)
                      ]),
                ),
              ),
            ),
          ),
        ),
        Container(
          width: 300,
          child: Padding(
            padding: const EdgeInsets.only(top: 1.8),
            child: Text(
              data["Title"]!,
              style: TextStyle(
                  color: AppColors.primaryText,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Container(
          width: 300,
          child: Padding(
            padding: const EdgeInsets.all(7.5),
            child: Text(
              data["Text"]!,
              style: TextStyle(
                  color: AppColors.primaryText,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
        )
      ],
    );
  }

  displayCards(restaurants) {
    List<Widget> L = [];
    List<Widget> rowList = [];
    int counter = 0;
    restaurants.forEach((restaurant) {
      String image = restaurant.image;
      String name = restaurant.name;
      String id = restaurant.id;
      String views = restaurant.distance.toString();
      if (image == "") {
        image = 'https://via.placeholder.com/50';
      }
      if (name == "") {
        name = "Nombre";
      }
      if (counter == 3) {
        counter = 0;
        L.add(Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: rowList,
          ),
        ));
        rowList = [];
      }
      rowList.add(_cardPlace(image, name, views, id, restaurant));
      counter += 1;
    });
    return L;
  }

  Widget floatingButton() {
    return Container(
      height: 100,
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.all(15),
      child: FloatingActionButton.extended(
        backgroundColor: AppColors.primary,
        label: const Text(
          "Ver en Mapa",
          style: TextStyle(fontSize: 18),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('/map');
        },
      ),
    );
  }

  Widget searchField(_searchController) {
    return Padding(
      padding: const EdgeInsets.only(left: 40, right: 40, top: 15, bottom: 50),
      child: TextField(
        controller: _searchController,
        decoration: InputDecoration(
          hintText: "Doña Blanca",
          hintStyle: TextStyle(
            fontSize: 20,
            color: AppColors.primaryText.withOpacity(0.6), // color del hint
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: const BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 35,
            vertical: 20,
          ),
          filled: true,
          fillColor: AppColors.secondary,
        ),
      ),
    );
  }

  Widget _cardPlace(image, rName, distance, id, r) {
    double borderRadiusInt = 15;
    return GestureDetector(
      onTap: () {
        services.addOneView(id);
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => RestaurantView(r: r)));
      },
      child: SizedBox(
        width: 110,
        child: Card(
          color: AppColors.loginText.withOpacity(0.5),
          elevation: 2.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadiusInt),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Material(
                  color: Colors.grey,
                  elevation: 10.0,
                  shadowColor: AppColors.primaryText.withOpacity(0.4),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(borderRadiusInt),
                    topRight: Radius.circular(borderRadiusInt),
                    bottomLeft: Radius.circular(borderRadiusInt),
                    bottomRight: Radius.circular(borderRadiusInt),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(borderRadiusInt),
                      topRight: Radius.circular(borderRadiusInt),
                      bottomLeft: Radius.circular(borderRadiusInt),
                      bottomRight: Radius.circular(borderRadiusInt),
                    ),
                    child: Image(
                      image: CachedNetworkImageProvider(image),
                      height: 110,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      rName,
                      style: const TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Text(
                      distance + " Km",
                      style: TextStyle(
                        fontSize: 8.0,
                        color: AppColors.primaryText.withOpacity(0.5),
                      ),
                    ),
                    const SizedBox(height: 8.0),
                    Text(
                      r.views.toString() + " views",
                      style: TextStyle(
                        fontSize: 8.0,
                        color: AppColors.primaryText.withOpacity(0.5),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _horizontalMenu() {
    List<Widget> L = [];
    for (int i = 0; i < opciones.length; i++) {
      String element = opciones[i];
      L.add(TextButton(
          onPressed: () {
            setState(() {
              _currentIndex = i;
              if (i == 0) {
                BlocProvider.of<RestaurantBloc>(context)
                    .add(RequestAllRestaurants());
              }
              if (i == 2) {
                BlocProvider.of<RestaurantBloc>(context)
                    .add(RequestDistancedRestaurants());
              }
              if (i == 3) {
                BlocProvider.of<RestaurantBloc>(context)
                    .add(RequestPopularRestaurants());
              }
              if (i == 1) {
                BlocProvider.of<RestaurantBloc>(context).add(RequestEvents());
              }
              BlocProvider.of<RestaurantBloc>(context)
                  .add(RequestAllRestaurants());
            });
          },
          child: Text(
            element,
            style: TextStyle(
              fontSize: 12,
              color: _currentIndex == i ? AppColors.primaryText : Colors.grey,
            ),
          )));
    }
    ;
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              color: AppColors.negativeSpace,
              width: 0,
            ),
          ),
        ),
        child: ButtonBar(
          alignment: MainAxisAlignment.spaceAround,
          children: L,
        ),
      ),
    );
  }

  _custoAppBar() {
    return PreferredSize(
      preferredSize: const Size.fromHeight(120),
      child: AppBar(
        iconTheme: IconThemeData(color: AppColors.primaryText),
        flexibleSpace: FlexibleSpaceBar(
          title: Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Tienes',
                  style: TextStyle(color: AppColors.primaryText, fontSize: 28),
                ),
                Text(
                  'ganas de comer?',
                  style: TextStyle(
                      color: AppColors.primaryText,
                      fontWeight: FontWeight.bold,
                      fontSize: 32),
                ),
              ],
            ),
          ),
        ),
        // The title text which will be shown on the action bar

        backgroundColor: AppColors.appBarColor,
        elevation: 0.0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Container(
                color: AppColors.negativeSpace,
                height: 20,
                width: 50,
                alignment: Alignment.center,
                child: GestureDetector(
                    child: CachedNetworkImage(
                        imageUrl: usuario.ppUrl == ""
                            ? 'https://i.pinimg.com/originals/68/16/3e/68163efb3c2201721d8e681cde6aef2b.jpg'
                            : usuario.ppUrl,
                        width: 200,
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                        fit: BoxFit.fitHeight),
                    onTap: () {
                      Provider.of<AccountViewModel>(context, listen: false)
                          .getUser(usuario.email);
                      Navigator.of(context).pushNamed('/account');
                    }),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
