import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:uni_advisor/model/models/microwave.dart';
import 'package:localstorage/localstorage.dart';

import '../model/repository/firebase_service_adapter.dart';

class MicrowaveService {
  final microwave_repository =
      FirebaseServiceAdapter(FirebaseFirestore.instance, "microwaves");
  final LocalStorage storage = LocalStorage('localstorage_app');

  List<Microwave> microwaves = [];

  dynamic helper(microwave) {
    print(microwave);
    List likes = microwave['likes'] as List;
    microwave['likes_number'] = likes.length;
    return microwave;
  }

  void addMicroWavesLocalStorage(listEvents) {
    final info = json.encode({'microwavesList': listEvents});
    storage.setItem('microwavesList', info);
  }

  Future<Microwave> getMicrowaveById(id) async {
    return Microwave.fromJson(await microwave_repository.getById(id));
  }

  List<Microwave> getMicrowavesFromLocalStorage() {
    var rawList = [];
    List<Microwave> microwaves = [];
    try {
      Map<String, dynamic> info = json.decode(storage.getItem('placesList'));
      rawList = info['microwavesList'];
      int topNumber = rawList.length;
      for (int i = 0; i < topNumber; i++) {
        microwaves.add(Microwave.fromJson(rawList[i]));
      }
    } catch (e) {
      print("Falló al cargar datos");
    }
    print("Microoondas");
    return microwaves;
  }

  Future<List<Microwave>> getAllMicrowaves() async {
    microwaves = [];
    Future<List> future_microwaves = microwave_repository.getAll();
    List json_microwaves = await future_microwaves;
    for (var microwave in json_microwaves) {
      microwaves.add(Microwave.fromJson(helper(microwave)));
    }
    addMicroWavesLocalStorage(microwaves);
    return microwaves;
  }

  Future<void> addLike(id, email) async {
    Microwave m = Microwave.fromJson(await microwave_repository.getById(id));
    if (m.likes != null) {
      if (!m.likes.contains(email)) {
        m.likes.add(email);
        microwave_repository.update(id, (m).toJsonSpanish());
      }
    }
  }

  Future<void> deleteLike(id, email) async {
    Microwave m = Microwave.fromJson(await microwave_repository.getById(id));
    print(m.toJsonSpanish());

    if (m.likes != null) {
      if (m.likes.contains(email)) {
        m.likes.remove(email);
        print(m.toJsonSpanish());

        microwave_repository.update(id, (m).toJsonSpanish());
      }
    }
  }

  Future<bool> alreadyLiked(String id, String email) async {
    Microwave m =
        await Microwave.fromJson(await microwave_repository.getById(id));
    if (m.likes != null) {
      if (!m.likes.contains(email)) {
        return true;
      }
    }
    return false;
  }

  Future<bool> alreadyLiked0(String id, String email) async {
    return await compute(_alreadyLikedIsolate, {'id': id, 'email': email});
  }

  Future<bool> _alreadyLikedIsolate(Map<String, dynamic> params) async {
    String id = params['id'];
    String email = params['email'];
    Microwave m = Microwave.fromJson(await microwave_repository.getById(id));
    if (m.likes != null) {
      if (!m.likes.contains(email)) {
        return true;
      }
    }
    return false;
  }
}
