import 'dart:async';

import 'package:light/light.dart';

class Light_Provider{ 
   late Light _light;
   late StreamSubscription _subscription;

  void onData(int luxValue) async {
    print("Lux value: $luxValue");
    
  }

  void stopListening() {
    _subscription.cancel();
  }

  void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (exception) {
      print(exception);
    }
  }
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    startListening();
  }


}