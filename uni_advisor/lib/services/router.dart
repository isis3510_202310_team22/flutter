import 'package:flutter/material.dart';
import 'package:uni_advisor/view/account_view.dart';
import 'package:uni_advisor/view/detail_microwave_view.dart';
import 'package:uni_advisor/view/home_view.dart';
import 'package:uni_advisor/view/map_view.dart';
import 'package:uni_advisor/view/microwave_view.dart';
import 'package:uni_advisor/view/login_view.dart';
import 'package:uni_advisor/view/mine_tutorial_view.dart';
import 'package:uni_advisor/view/teacher_view.dart';
import 'package:uni_advisor/view/tutorial_view.dart';

import '../configs/colors.dart';

//This class is used to make te routing of the app.
class AppRouter {
  MaterialPageRoute? onGenerateRoute(RouteSettings settings) {
    AppColors.updateColors();
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => LoginView());
      case '/account':
        return MaterialPageRoute(builder: (_) => AccountView());
      case '/home':
        return MaterialPageRoute(builder: (_) => HomeView());
      case '/map':
        return MaterialPageRoute(builder: (_) => MapView());
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginView());
      case '/microwave':
        return MaterialPageRoute(builder: (_) => MicrowaveView());
      case '/teachers':
        return MaterialPageRoute(builder: (_) => TeacherView());
      case '/detailMicrowave':
        return MaterialPageRoute(builder: (_) => DetailMicrowaveView());
      case '/tutorials':
        return MaterialPageRoute(builder: (_) => TutorialView());
      case '/mine_tutorials':
        return MaterialPageRoute(builder: (_) => MineTutorialView());
      default:
        return null;
    }
  }
}
