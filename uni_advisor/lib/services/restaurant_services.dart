import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uni_advisor/model/models/restaurant.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';
import 'package:uni_advisor/services/location_service.dart';

class RestaurantServices {
  final restaurantRepository =
      FirebaseServiceAdapter(FirebaseFirestore.instance, "restaurantes");
  final aa = FirebaseFirestore.instance.collection("restaurantes");
  final locationServcie = LocationService();
  List<Restaurant> L = [];

  Future<List<Restaurant>> getAllRestaurantsD() async {
    List<Restaurant> r = [];
    Future<List> futureJsonRestaurants = restaurantRepository.getAll();
    List jsonRestaurants = await futureJsonRestaurants;
    List idList = await restaurantRepository.getAllIds();
    int counter = 0;
    for (var restaunrant in (jsonRestaurants)) {
      r.add(Restaurant.fromJson(restaunrant));
      counter += 1;
    }
    return r;
  }

  Future<List<Restaurant>> sortRestaurantesByView() async {
    if (L.isEmpty) {
      L = await getAllRestaurants();
    }
    L.sort((a, b) => b.views.compareTo(a.views));
    return L;
  }

  Future<List<Restaurant>> sortRestaurantesByDistance() async {
    if (L.isEmpty) {
      L = await getAllRestaurants();
    }
    L.sort((a, b) => a.distance.compareTo(b.distance));
    return L;
  }

  Future<void> addOneView(id) async {
    // ignore: unused_local_variable
    Restaurant r = Restaurant.fromJson(await restaurantRepository.getById(id));
    int views = r.views + 1;
    aa.doc(id).update({"views": views});
  }

// Micro-Optimization made:
// Instead of getting the user location for each restaurant, it is only obtained once and calculate distance for all when they are
  Future<String> calculateDistance(Restaurant r) async {
    String coordinates = await locationServcie.getLocation();
    double userLat = double.parse(coordinates.split(",")[0]);
    double userLong = double.parse(coordinates.split(",")[1]);
    String distance = locationServcie.CalculateDistance(
        userLat, userLong, r.location.latitude, r.location.longitude);
    return distance;
  }

  Future<List<Restaurant>> getAllRestaurants() async {
    String coordinates = await locationServcie.getLocation();
    List<Restaurant> r = [];
    Future<List> futureJsonRestaurants = restaurantRepository.getAll();
    List jsonRestaurants = await futureJsonRestaurants;
    List idList = await restaurantRepository.getAllIds();
    int counter = 0;
    double userLat = double.parse(coordinates.split(",")[0]);
    double userLong = double.parse(coordinates.split(",")[1]);

    for (var restaunrant in (jsonRestaurants)) {
      GeoPoint location = restaunrant['location'] != null
          ? restaunrant['location'] as GeoPoint
          : const GeoPoint(0, 0);
      restaunrant['id'] = idList[counter];
      restaunrant['distance'] = locationServcie.CalculateDistance(
          userLat, userLong, location.latitude, location.longitude);
      r.add(Restaurant.fromJson(restaunrant));
      counter += 1;
    }
    L = r;

    return r;
  }
}
