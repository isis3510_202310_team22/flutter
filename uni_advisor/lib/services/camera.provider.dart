import 'dart:io';
import 'package:image_picker/image_picker.dart';

class CameraProvider {

  Future pickImage(ImageSource source) async {
      XFile? imageX = await ImagePicker().pickImage(source: source);
      if (imageX == null) return;
      File image = File(imageX.path);
    return image;
  }

}