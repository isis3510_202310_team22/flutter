import 'dart:convert';

import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_advisor/configs/constants.dart';

class PreferenceManager {
  static const String USER_KEY = "user";

  static const String MICROWAVE_KEY = "microwave";

  static Future<bool> saveUser(Map<String, dynamic> user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String userJson = json.encode(user);
    return prefs.setString(USER_KEY, userJson);
  }

  static Future<Map<String, dynamic>?> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userJson = prefs.getString(USER_KEY);
    if (userJson != null) {
      return json.decode(userJson);
    }
    return null;
  }

  static Future<bool> removeUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(USER_KEY);
  }

  static Future<Map<String, dynamic>?> getMicrowave() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? userJson = prefs.getString(MICROWAVE_KEY);
    if (userJson != null) {
      return json.decode(userJson);
    }
    return null;
  }

  static Future<bool> saveMicrowave(Map<String, dynamic> micro) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String microJson = json.encode(micro);
    return prefs.setString(USER_KEY, microJson);
  }

  static Future<bool> removeMicrowave() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(MICROWAVE_KEY);
  }

  static Future<List<PointLatLng>> getPolylinePoints(
      double lat1, double lng1, double lat2, double lng2) async {
    final key = 'polyline_${lat2}_${lng2}';
    print('maptest___${key}');
    // Check if the points are already cached
    final prefs = await SharedPreferences.getInstance();
    try {
      // Fetch the points and cache them
      PolylinePoints polylinePoints = PolylinePoints();
      PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        google_api_key, // Replace with your API key
        PointLatLng(lat1, lng1),
        PointLatLng(lat2, lng2),
      );
      // Store the points in the cache
      final pointCoords = result.points
          .map((point) => '${point.latitude},${point.longitude}')
          .toList();
      await prefs.setStringList(key, pointCoords);

      // Return the fetched points
      print('maptest retrieved from web');
      return result.points;
    } catch (e) {
      final cachedPoints = prefs.getStringList(key);

      if (cachedPoints != null) {
        // Return the cached points
        print('maptest retrieved from cache');
        return cachedPoints.map((coords) {
          final parts = coords.split(',');
          final lat = double.parse(parts[0]);
          final lng = double.parse(parts[1]);
          return PointLatLng(lat, lng);
        }).toList();
      } else {
        // No cached points available
        print('maptest retrieved no cached points');
        return [];
      }
    }
  }
}
