import 'package:location/location.dart';
import 'dart:math' show cos, sqrt, asin;



class LocationService {
  Location location = Location();

  Future<String> getLocation() async {
    bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
    }
    PermissionStatus permissionGranted = await location.hasPermission();

    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
    }

    var currentLocation = await location.getLocation();    
    String q = "${currentLocation.latitude},${currentLocation.longitude}";
    return q;
 }

  Future<LocationData> getCurrentLocation() async {
    bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
    }
    PermissionStatus permissionGranted = await location.hasPermission();

    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
    }

    return await location.getLocation();    
 }

 String CalculateDistance(Userlat, Userlong, placeLat, placeLong){
    var p = 0.017453292519943295;
    var a = 0.5 - cos((placeLat - Userlat) * p)/2 + 
          cos(Userlat * p) * cos(placeLat * p) * 
          (1 - cos((placeLong - Userlong) * p))/2;
    return (12742 * asin(sqrt(a))).toStringAsFixed(2);
  }

}
