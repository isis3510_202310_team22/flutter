import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uni_advisor/model/models/teacher.dart';
import '../model/repository/firebase_service_adapter.dart';

class TeacherService {
  final teacher_repository =
      FirebaseServiceAdapter(FirebaseFirestore.instance, "teachers");

  List<Teacher> teachers = [];

  Future<List<Teacher>> getAllTeachers() async {
    teachers = [];
    Future<List> future_teachers = teacher_repository.getAll();
    List json_teachers = await future_teachers;
    for (var teacher in json_teachers) {
      teachers.add(Teacher.fromJson(teacher));
    }
    return teachers;
  }
}
