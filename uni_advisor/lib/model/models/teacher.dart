class Teacher {
  final String id;
  final String email;
  final String name;
  final String university;
  final String career;
  final String mainCourse;
  final String urlImage;

  const Teacher(
      {required this.id,
      required this.email,
      required this.name,
      required this.university,
      required this.career,
      required this.mainCourse,
      required this.urlImage});

  factory Teacher.fromJson(Map<String, dynamic> json) => Teacher(
      id: !json.containsKey('id') ? '' : json["id"],
      email: !json.containsKey('email') ? '' : json["email"],
      name: !json.containsKey('name') ? '' : json["name"],
      university: !json.containsKey('university') ? '' : json["university"],
      career: !json.containsKey('career') ? '' : json["career"],
      mainCourse: !json.containsKey('mainCourse') ? '' : json["mainCourse"],
      urlImage: !json.containsKey('urlImage') ? '' : json["urlImage"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "name": name,
        "university": university,
        "career": career,
        "mainCourse": mainCourse,
      };
}
