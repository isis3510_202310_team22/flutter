class Comment {
  final String id;
  final String idTeacher;
  final String idUser;
  final String text;
  final double rating;

  const Comment(
      {required this.id,
      required this.idTeacher,
      required this.idUser,
      required this.text,
      required this.rating});

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
      id: !json.containsKey('id') ? '' : json["id"],
      idTeacher: !json.containsKey('idTeacher') ? '' : json["idTeacher"],
      idUser: !json.containsKey('idUser') ? '' : json["idUser"],
      text: !json.containsKey('text') ? '' : json["text"],
      rating: !json.containsKey('rating') ? 0.0 : json["rating"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "idTeacher": idTeacher,
        "idUser": idUser,
        "text": text,
        "rating": rating
      };
}
