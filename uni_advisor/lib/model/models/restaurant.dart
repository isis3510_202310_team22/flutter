import 'dart:ffi';
import 'package:cloud_firestore/cloud_firestore.dart';

class Restaurant {
  final String id;
  final String address;
  final String image;
  final GeoPoint location;
  final double distance;
  final String name;
  final List reviews;
  final List score;
  final String type;
  final int views;
  final String latitud;
  final String longitud;

  Restaurant(
      {required this.id,
      required this.address,
      required this.image,
      required this.location,
      required this.distance,
      required this.name,
      required this.reviews,
      required this.score,
      required this.type,
      required this.views,
      required this.latitud,
      required this.longitud});

  factory Restaurant.fromJson(Map<String, dynamic> json) {
    return Restaurant(
      id: json['id'] != null ? json['id'] as String : "",
      address: json['address'] != null ? json['address'] as String : "",
      image: json['image'] != null ? json['image'] as String : "",
      location: json['location'] != null
          ? json['location'] as GeoPoint
          : GeoPoint(0, 0),
      distance: json['distance'] != null ? double.parse(json['distance']) : 0,
      name: json['name'] != null ? json['name'] as String : "",
      reviews: json['reviews'] != null ? json['reviews'] as List : [],
      score: json['score'] != null ? json['score'] as List : [],
      type: json['type'] != null ? json['type'] as String : "",
      views: json['views'] != null ? json['views'] as int : 0,
      latitud: json['latitud'] != null ? json['latitud'] as String : "",
      longitud: json['longitud'] != null ? json['longitud'] as String : "",
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'address': address,
      'image': image,
      'location': location,
      'distance': distance,
      'name': name,
      'reviews': reviews,
      'score': score,
      'type': type,
      'views': views,
      'latitud': latitud,
      'longitud': longitud
    };
  }
}
