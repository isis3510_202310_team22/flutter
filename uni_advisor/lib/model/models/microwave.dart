class Microwave {
  final String id;
  final int quantity;
  final String edifice;
  final String busy_schedule;
  final String empy_schedule;
  final String image;
  final String floor;
  final dynamic likes;
  final String descriptive_location;
  final int likes_number;

  Microwave(
      {required this.id,
      required this.quantity,
      required this.edifice,
      required this.busy_schedule,
      required this.empy_schedule,
      required this.image,
      required this.floor,
      required this.likes,
      required this.descriptive_location,
      required this.likes_number});

  factory Microwave.fromJson(Map<String, dynamic> json) {
    return Microwave(
      id: json['id'] ?? "",
      quantity: json['Cantidad'] ?? 0,
      edifice: json['Edificio'] ?? "",
      busy_schedule: json['HorariosConcurridos'] ?? "",
      empy_schedule: json['HorariosDisponibles'] ?? "",
      image: json['Imagen'] ?? "",
      floor: json['Piso'] != null ? json['Piso'].toString() : "",
      likes: json['likes'] ?? [],
      descriptive_location: json['UbicacionDescriptiva'] ?? "",
      likes_number: json['likes_number'] != null ? (json['likes_number']) : 0,
    );
  }
  Map<String, dynamic> toJsonSpanish() {
    return {
      'id': id,
      'Cantidad': quantity,
      'Edificio': edifice,
      'HorariosConcurridos': busy_schedule,
      'HorariosDisponibles': empy_schedule,
      'Imagen': image,
      'Piso': floor,
      'likes': likes,
      'UbicacionDescriptiva': descriptive_location,
      'likes_number': likes.length,
    };
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'quantity': quantity,
      'edifice': edifice,
      'busy_schedule': busy_schedule,
      'empy_schedule': empy_schedule,
      'image': image,
      'floor': floor,
      'likes': likes,
      'descriptive_location': descriptive_location,
      'likes_number': likes.length,
    };
  }
}
