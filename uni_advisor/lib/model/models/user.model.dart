import 'package:equatable/equatable.dart';

class User extends Equatable {
  String id;
  String email;
  String name;
  String phoneNumber;
  String university;
  String career;
  String semester;
  String ppUrl;

  User({required this.id, required this.email, required this.name, required this.phoneNumber, required this.university, required this.career, required this.semester, required this.ppUrl});

  static var empty = User(id: '', email: '', name: '', phoneNumber: '', university:'', career: '', semester: '', ppUrl:'');

  bool get isEmpty => this == User.empty;
  bool get isNotEmpty => this != User.empty;

  @override
  List<Object?> get props => [id, email, name, phoneNumber, university, career, semester, ppUrl];

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: !json.containsKey('id')? '' : json["id"],
    email: !json.containsKey('email')? '' : json["email"],
    name: !json.containsKey('name')? '' : json["name"],
    phoneNumber: !json.containsKey('phoneNumber')? '' : json["phoneNumber"],
    university: !json.containsKey('university')? '' : json["university"],
    career: !json.containsKey('career')? '' : json["career"],
    semester: !json.containsKey('semester')? '' : json["semester"],
    ppUrl: !json.containsKey('ppUrl')? '' : json["ppUrl"]

  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "email": email,
    "name": name,
    "phoneNumber": phoneNumber,
    "university": university,
    "career": career,
    "semester": semester,
    "ppUrl": ppUrl
  };

}