class Tutorial {
  final String id;
  final String course;
  final int places;
  final String room;
  final String date;
  final dynamic attendants;

  const Tutorial(
      {required this.id,
      required this.course,
      required this.places,
      required this.room,
      required this.attendants,
      required this.date});

  factory Tutorial.fromJson(Map<String, dynamic> json) => Tutorial(
      id: !json.containsKey('id') ? '' : json["id"],
      course: !json.containsKey('course') ? '' : json["course"],
      places: !json.containsKey('places') ? 0 : json["places"],
      room: !json.containsKey('room') ? '' : json["room"],
      date: !json.containsKey('date') ? '' : json["date"],
      attendants: json['attendants'] ?? []);

  Map<String, dynamic> toJson() => {
        "id": id,
        "course": course,
        "places": places,
        "room": room,
        "attendants": attendants,
        "date": date
      };
}
