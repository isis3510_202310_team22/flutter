import 'package:http/http.dart' as http;
import 'package:html/parser.dart' as parser;
import 'package:html/dom.dart';

class Scraper {
  String baseUrl;

  Scraper(this.baseUrl);

  Future<List<Element>> getElements(String selector) async {
    var url = baseUrl;
    var response = await http.get(Uri.parse(url));
    var document = parser.parse(response.body);
    return document.querySelectorAll(selector);
  }

  Future<List> getTitles() async {
    var url = baseUrl;
    var response = await http.get(Uri.parse(url));
    var document = parser.parse(response.body);
    var titles = document.querySelectorAll('h3.title');
    var ret = [];
    for (var title in titles) {
      ret.add(title.text);
    }
    return ret;
  }

  Future<List> getImages() async {
    var url = baseUrl;
    var response = await http.get(Uri.parse(url));
    var document = parser.parse(response.body);
    var divs = document.querySelectorAll('div.item__evento');
    var ret = [];
    for (var div in divs) {
      var figure = div.querySelector('img');
      var src = figure?.attributes["src"];
      ret.add(src);
    }
    return ret;
  }

  Future<List> getTexts() async {
    var url = baseUrl;
    var response = await http.get(Uri.parse(url));
    var document = parser.parse(response.body);
    var divs = document.querySelectorAll('div.dest__evento');
    var ret = [];
    for (var i = 0; i < divs.length; i += 3) {
      var text = "${divs[i].text}\n${divs[i + 1].text}\n${divs[i + 2].text}";
      ret.add(text);
    }
    return ret;
  }

  Future<List> getRedirects() async {
    var url = baseUrl;
    var response = await http.get(Uri.parse(url));
    var document = parser.parse(response.body);
    var divs = document.querySelectorAll('a.btn.btn-primary-eventtia');
    var ret = [];
    for (var div in divs) {
      var src = div.attributes["href"];
      ret.add(src);
    }
    return ret;
  }
}
