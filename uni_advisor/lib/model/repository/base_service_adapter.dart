abstract class BaseServiceAdapter<T> {
  Future<List<T>> getAll();
  Future<List<T>> getAllIds();
  Future<T> getById(String id);
  Future<void> add(Map<String, dynamic> item, id);
  Future<void> update(String id, Map<String, dynamic> item);
  Future<void> delete(String id);
  void create(Map<String, dynamic> item);
}
