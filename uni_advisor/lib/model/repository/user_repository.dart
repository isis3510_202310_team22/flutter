import 'package:uni_advisor/model/models/user.model.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';
import 'package:uni_advisor/services/preference_manager.dart';

abstract class UserRepository {
  Future<User> getUser(String emailId);

  Future<void> saveUser(User user);
}

class UserDefaultRepository extends UserRepository {
  UserDefaultRepository({required this.userProvider});

  final FirebaseServiceAdapter userProvider;

  @override
  Future<User> getUser(emailId) async {
    final hasCachedData = await PreferenceManager.getUser() != null;
    if (!hasCachedData) {
      final user = await userProvider.getById(emailId);
      await PreferenceManager.saveUser(user);
    }

    final user = await PreferenceManager.getUser();
    return User.fromJson(user!);
  }

  @override
  Future<void> saveUser(User user) async {
    PreferenceManager.saveUser(user.toJson());
    userProvider.update(user.email, user.toJson());
  }

}
