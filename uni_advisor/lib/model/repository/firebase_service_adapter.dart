import 'package:cloud_firestore/cloud_firestore.dart';

import 'base_service_adapter.dart';

class FirebaseServiceAdapter<T> implements BaseServiceAdapter<T> {
  final FirebaseFirestore firestore;
  final String collectionName;

  FirebaseServiceAdapter(this.firestore, this.collectionName);

  @override
  Future<List<T>> getAll() async {
    final snapshot = await firestore.collection(collectionName).get();
    return snapshot.docs.map((doc) => doc.data() as T).toList();
  }

  @override
  Future<List<T>> getAllIds() async {
    final snapshot = await firestore.collection(collectionName).get();
    return snapshot.docs.map((doc) => doc.id as T).toList();
  }

  @override
  Future<T> getById(String id) async {
    final snapshot = await firestore.collection(collectionName).doc(id).get();
    return snapshot.data() as T;
  }

  @override
  Future<void> add(Map<String, dynamic> item, id) async {
    if (item != null) {
      await firestore.collection(collectionName).doc(id).set(item);
    }
  }

  @override
  Future<void> update(String id, Map<String, dynamic> item) async {
    await firestore.collection(collectionName).doc(id).update(item);
  }

  @override
  Future<void> delete(String id) async {
    await firestore.collection(collectionName).doc(id).delete();
  }

  @override
  void create(Map<String, dynamic> item) async {
    await firestore.collection(collectionName).add(item);
  }
}
