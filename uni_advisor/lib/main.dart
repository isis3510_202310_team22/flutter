import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:uni_advisor/configs/colors.dart';
import 'package:uni_advisor/model/repository/authentication_repository.dart';
import 'package:uni_advisor/services/router.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:uni_advisor/viewModel/bloc/blocAuthentication/authentication_bloc.dart';
import 'package:uni_advisor/viewModel/bloc/blocMap/map_bloc.dart';
import 'package:uni_advisor/viewModel/bloc/blocMicrowaves/microwave_bloc.dart';
import 'package:uni_advisor/viewModel/bloc/blocRestaurants/restaurant_bloc.dart';
import 'package:uni_advisor/viewModel/account_view_model.dart';
import 'package:uni_advisor/viewModel/bloc/blocTeacher/teacher_bloc.dart';
import 'package:uni_advisor/viewModel/cubit/cubitConnectivity/connectivity_cubit.dart';
import 'firebase_options.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  AppColors.updateColors();
  EquatableConfig.stringify = kDebugMode;

  await SentryFlutter.init(
    (options) {
      options.dsn =
          'https://d2b876a0d2594a6aa55b7aab2ff3e192@o4504901449744384.ingest.sentry.io/4504901450858496';
      options.enableAutoSessionTracking = true;

      options.autoSessionTrackingInterval = const Duration(milliseconds: 60000);
      // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
      // We recommend adjusting this value in production.
      options.tracesSampleRate = 1.0;
      options.attachScreenshot = true;
      options.autoAppStart = true;
      options.enableAutoPerformanceTracing = true;
      options.tracesSampler = (samplingContext) {
        // return a number between 0 and 1 or null (to fallback to configured value)
      };
    },
    appRunner: () => runApp(AppView()),
  );

  //await Sentry.captureMessage('Otro');
}

class AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final AppRouter _router = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => AccountViewModel())],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
              create: (context) => AuthenticationBloc(
                  authenticationRepository: AuthenticationRepository())),
          BlocProvider<RestaurantBloc>(
            create: (context) => RestaurantBloc(),
          ),
          BlocProvider(create: (context) => ConnectivityCubit()),
          BlocProvider(create: (context) => MicrowaveBloc()),
          BlocProvider(create: (context) => TeacherBloc()),
          BlocProvider(create: (context) => MapBloc())
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          navigatorObservers: [SentryNavigatorObserver()],
          onGenerateRoute: _router.onGenerateRoute,
        ),
      ),
    );
  }
}
