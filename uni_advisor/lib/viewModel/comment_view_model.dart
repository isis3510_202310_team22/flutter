import 'dart:isolate';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uni_advisor/model/models/comment.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';
import 'package:uni_advisor/view/teacher_detail_view.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class CommentViewModel {
  CommentViewModel();
  final comment_repository =
      FirebaseServiceAdapter(FirebaseFirestore.instance, "comments");
  RootIsolateToken rootIsolateToken = RootIsolateToken.instance!;
  List<Comment> comments = [];

  void getAllCommentsIsolate(List<dynamic> values) async {
    SendPort sendPort = values[0];
    BackgroundIsolateBinaryMessenger.ensureInitialized(rootIsolateToken);
    comments = [];
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      Future<List> future_comments = comment_repository.getAll();
      for (var element in await future_comments) {
        comments.add(Comment.fromJson(element));
      }
      createList(comments);
      sendPort.send(comments);
    } else {
      sendPort.send(await loadList());
    }
  }

  Future<List<Comment>> getAllComments() async {
    ReceivePort port = ReceivePort();
    final isolate = await Isolate.spawn<List<dynamic>>(
        getAllCommentsIsolate, [port.sendPort]);
    final result = await port.first;
    return result;
  }

  Future<void> saveComment(Comment comment) async {
    await comment_repository.add(
        comment.toJson(), comment.idUser + "_" + comment.idTeacher);
  }

  Future<List<Comment>> filterComment(
      List<Comment> comments, String teacherEmail) async {
    List<Comment> ret = [];
    for (var comment in comments) {
      if (teacherEmail == comment.idTeacher) {
        ret.add(comment);
      }
    }
    return ret;
  }

  Future<List<Comment>> loadList() async {
    List<Map<String, dynamic>> a = await getAllData();
    List<Comment> ret = [];
    var cont = 0;
    for (var element in a) {
      print(element["id"]);
      cont += 1;
      ret.add(Comment(
          id: element["id"],
          idTeacher: element["idTeacher"],
          idUser: element["idUser"],
          text: element["text"],
          rating: element["rating"]));
    }
    return ret;
  }

  Future<void> createList(List<Comment> list) async {
    await deleteAllData();
    for (var element in list) {
      await insertData(element.id, element.idTeacher, element.idUser,
          element.text, element.rating);
    }
  }

  Future<Database> openDataBase() async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, 'comments.db');
    return openDatabase(path, version: 1, onCreate: (db, version) {
      return db.execute(
        'CREATE TABLE IF NOT EXISTS my_table (id TEXT PRIMARY KEY, idTeacher TEXT, idUser TEXT, text TEXT,rating REAL)',
      );
    });
  }

  Future<void> insertData(String id, String idTeacher, String idUser,
      String text, double rating) async {
    final db = await openDataBase();
    await db.insert('my_table', {
      'id': id,
      'idTeacher': idTeacher,
      'idUser': idUser,
      'text': text,
      "rating": rating
    });
  }

  Future<void> deleteAllData() async {
    final db = await openDataBase();
    await db.delete('my_table');
  }

  Future<List<Map<String, dynamic>>> getAllData() async {
    final db = await openDataBase();
    return db.query('my_table');
  }
}
