part of 'map_bloc.dart';

@immutable
abstract class MapState extends Equatable {
  const MapState();

  @override
  List<Object> get props => [];
}

class MapInitial extends MapState {}

class MapLoading extends MapState {}

class MapLoaded extends MapState {

  final LocationData location;
  final List<Restaurant> restaurants;

  const MapLoaded({required this.location, required this.restaurants});

  @override
  List<Object> get props => [location, restaurants];
  

}
