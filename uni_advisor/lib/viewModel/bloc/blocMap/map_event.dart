part of 'map_bloc.dart';

@immutable
abstract class MapEvent extends Equatable{
  const MapEvent();

  @override
  List<Object> get props => [];
}

class LoadMap extends MapEvent {}

class SelectRestaurant extends MapEvent {}

class FindPath extends MapEvent {}

class LoadRestaurants extends MapEvent {}


