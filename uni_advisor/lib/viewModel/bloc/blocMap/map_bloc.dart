import 'dart:isolate';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';
import 'package:uni_advisor/model/models/restaurant.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';
import 'package:uni_advisor/services/location_service.dart';
import 'package:uni_advisor/services/restaurant_services.dart';
part 'map_event.dart';
part 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  final LocationService locationService = LocationService();
  final RestaurantServices restaurantServices = RestaurantServices();
  _createMapRender() {
      final FirebaseServiceAdapter firebaseServiceAdapter =
      FirebaseServiceAdapter(FirebaseFirestore.instance, 'map_renders');

    final now = DateTime.now();
    firebaseServiceAdapter.create({
      'timestamp': now,
    });
  }

  MapBloc() : super(MapInitial()) {
    on<LoadMap>((event, emit) async {
      emit(MapLoading());

      LocationData location = await locationService.getCurrentLocation();
      List<Restaurant> restaurants =
          await restaurantServices.getAllRestaurants();
      _createMapRender();
      emit(MapLoaded(location: location, restaurants: restaurants));
    });
  }
}
