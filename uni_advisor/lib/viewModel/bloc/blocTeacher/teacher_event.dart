part of 'teacher_bloc.dart';

@immutable
abstract class TeacherEvent {
  @override
  List<Object> get props => [];
}

class RequestAllTeachers extends TeacherEvent {}
