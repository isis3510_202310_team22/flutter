import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:uni_advisor/model/models/teacher.dart';
import 'package:uni_advisor/services/teacher_service.dart';
import 'package:uni_advisor/viewModel/bloc/blocMicrowaves/microwave_bloc.dart';

part 'teacher_event.dart';
part 'teacher_state.dart';

class TeacherBloc extends Bloc<TeacherEvent, TeacherState> {
  TeacherService services = TeacherService();
  TeacherBloc() : super(TeacherLoading()) {
    on<RequestAllTeachers>((event, emit) async {
      try {
        emit(TeacherLoading());
        List<Teacher> teachers = await services.getAllTeachers();
        emit(TeachersObtained(teachers));
      } catch (e) {
        print(e.toString());
      }
    });
  }
}
