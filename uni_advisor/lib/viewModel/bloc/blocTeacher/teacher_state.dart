part of 'teacher_bloc.dart';

@immutable
abstract class TeacherState {}

class TeacherLoading extends TeacherState {}

class TeachersObtained extends TeacherState {
  final List<Teacher> teachers;
  TeachersObtained(this.teachers);
  @override
  List<Object?> get props => [];
}
