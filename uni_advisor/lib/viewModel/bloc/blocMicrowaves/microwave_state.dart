part of 'microwave_bloc.dart';

@immutable
abstract class MicrowaveState {}

class MicrowaveLoading extends MicrowaveState {}

class MicrowaveFromLocal extends MicrowaveState {}

class AllMicrowavesObtained extends MicrowaveState {
  final List<Microwave> microwaves;
  AllMicrowavesObtained(this.microwaves);
  @override
  List<Object?> get props => [];
}
