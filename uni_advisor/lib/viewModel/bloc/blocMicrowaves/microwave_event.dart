part of 'microwave_bloc.dart';

@immutable
abstract class MicrowaveEvent {
  @override
  List<Object> get props => [];
}

class RequestAllMicrowaves extends MicrowaveEvent {}
