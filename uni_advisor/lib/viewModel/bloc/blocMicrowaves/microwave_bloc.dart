import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:uni_advisor/model/models/microwave.dart';

import '../../../services/microwave_service.dart';

part 'microwave_event.dart';
part 'microwave_state.dart';

class MicrowaveBloc extends Bloc<MicrowaveEvent, MicrowaveState> {
  MicrowaveService services = MicrowaveService();
  MicrowaveBloc() : super(MicrowaveLoading()) {
    on<RequestAllMicrowaves>(((event, emit) async {
      try {
        emit(MicrowaveLoading());
        if (services.getMicrowavesFromLocalStorage().isNotEmpty) {
          List<Microwave> microwaves = services.getMicrowavesFromLocalStorage();
          print("Obtenido de local storage");
          emit(MicrowaveFromLocal());
          emit(AllMicrowavesObtained(microwaves));
        } else {
          print("Obtenido 2 de local storage");

          List<Microwave> microwaves = await services.getAllMicrowaves();
          emit(AllMicrowavesObtained(microwaves));
        }
      } catch (e) {
        print(e.toString());
      }
    }));
  }
}
