part of 'restaurant_bloc.dart';

@immutable
abstract class RestaurantState {}

class RestaurantsLoading extends RestaurantState {}

class AllRestaurantsObtained extends RestaurantState {
  final List<Restaurant> restaurants;
  AllRestaurantsObtained(this.restaurants);

  @override
  List<Object?> get props => [];
}

class PopularRestaurantsObtained extends RestaurantState {
  final List<Restaurant> restaurants;
  PopularRestaurantsObtained(this.restaurants);

  @override
  List<Object?> get props => [];
}

class DistancedRestaurantsObtained extends RestaurantState {
  final List<Restaurant> restaurants;
  DistancedRestaurantsObtained(this.restaurants);
}

class EventsObtained extends RestaurantState {
  final List<Map<String, String>> events;
  EventsObtained(this.events);
  @override
  List<Object?> get props => [];
}
