part of 'restaurant_bloc.dart';

@immutable
abstract class RestaurantEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RequestAllRestaurants extends RestaurantEvent {}

class RequestPopularRestaurants extends RestaurantEvent {}

class RequestDistancedRestaurants extends RestaurantEvent {}

class RequestEvents extends RestaurantEvent {}
