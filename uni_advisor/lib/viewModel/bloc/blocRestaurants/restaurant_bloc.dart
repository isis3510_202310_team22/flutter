import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:uni_advisor/model/models/restaurant.dart';
import 'package:bloc/bloc.dart';
import 'package:uni_advisor/services/restaurant_services.dart';
import 'package:uni_advisor/viewModel/scraping_view_model.dart';

part 'restaurant_event.dart';
part 'restaurant_state.dart';

class RestaurantBloc extends Bloc<RestaurantEvent, RestaurantState> {
  RestaurantServices services = RestaurantServices();
  RestaurantBloc() : super(RestaurantsLoading()) {
    on<RequestAllRestaurants>((event, emit) async {
      try {
        emit(RestaurantsLoading());
        List<Restaurant> L = await services.getAllRestaurants();
        emit(AllRestaurantsObtained(L));
      } catch (e) {
        print(e.toString());
      }
    });

    on<RequestPopularRestaurants>((event, emit) async {
      try {
        emit(RestaurantsLoading());
        List<Restaurant> li = await services.sortRestaurantesByView();
        print(li);
        emit(PopularRestaurantsObtained(li));
      } catch (e) {
        print(e.toString());
      }
    });
    on<RequestDistancedRestaurants>((event, emit) async {
      try {
        emit(RestaurantsLoading());
        List<Restaurant> li = await services.sortRestaurantesByDistance();
        print(li);
        emit(DistancedRestaurantsObtained(li));
      } catch (e) {
        print(e.toString());
      }
    });
    on<RequestEvents>((event, emit) async {
      try {
        emit(RestaurantsLoading());
        var svm = ScrapingViewModel(
            "https://decanaturadeestudiantes.uniandes.edu.co/eventtia");
        print("____________________Se emitio el primer estado________________");
        List<Map<String, String>>? events = await svm.getElements();
        emit(EventsObtained(events!));
      } catch (e) {
        print(e.toString());
      }
    });
  }
}
