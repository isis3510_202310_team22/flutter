import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:uni_advisor/model/models/user.dart';
import 'package:uni_advisor/model/repository/authentication_repository.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';

import '../../../services/preference_manager.dart';

import '../../../services/preference_manager.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  FirebaseServiceAdapter userRepository =
      FirebaseServiceAdapter(FirebaseFirestore.instance, "users");
  AuthenticationBloc(
      {required AuthenticationRepository authenticationRepository})
      : super(const AuthenticationState.unknown()) {
    on<LoginRequested>((event, emit) async {
      emit(AuthenticationState.unknown());
      RegExp pattern = RegExp(r"@[a-z]+\.[a-z]+");
      var rightEmail = pattern.hasMatch(event.email);
      var fullBlanks = event.email != "" && event.password != "";
      if (!fullBlanks) {
        emit(AuthenticationState.unauthenticated(
            "Por favor digite todos los campos"));
      } else if (!rightEmail) {
        emit(AuthenticationState.unauthenticated("El correo está mal escrito"));
      } else {
        try {
          await AuthenticationRepository().logInWithEmailAndPassword(
              email: event.email, password: event.password);
          User _user = User.fromJson(await userRepository.getById(event.email));
          await PreferenceManager.saveUser(_user.toJson());
          emit(AuthenticationState.authenticated(_user));
        } catch (LogInWithEmailAndPasswordFailure) {
          emit(AuthenticationState.unauthenticated(
              "Verifique el correo y la contraseña"));
        }
      }
    });

    on<SignUpRequested>((event, emit) async {
      RegExp pattern = RegExp(r"@[a-z]+\.[a-z]+");
      emit(AuthenticationState.unknown());
      var fullBlanks = event.name != "" &&
          event.email != "" &&
          event.password != "" &&
          event.passwordx2 != "";
      var rightEmail = pattern.hasMatch(event.email);
      var rightPassword = event.password == event.passwordx2;
      var rightPasswordLen = event.password.length >= 6;
      if (!fullBlanks) {
        emit(AuthenticationState.unauthenticated(
            "Por favor digite todos los campos"));
      } else if (!rightEmail) {
        emit(AuthenticationState.unauthenticated("El correo no es válido"));
      } else if (!rightPasswordLen) {
        emit(AuthenticationState.unauthenticated("La contraseña es muy corta"));
      } else if (!rightPassword) {
        emit(AuthenticationState.unauthenticated(
            "La contraseña y su verificación son diferentes"));
      } else {
        try {
          await AuthenticationRepository()
              .signUp(email: event.email, password: event.password);
          User _user = User(
              id: event.email,
              email: event.email,
              name: event.name,
              phoneNumber: "",
              university: "",
              career: "",
              semester: "",
              ppUrl: "");
          await userRepository.add(_user.toJson(), _user.email);
          await PreferenceManager.saveUser(_user.toJson());

          emit(AuthenticationState.authenticated(_user));
        } on SignUpFailure catch (e) {
          emit(AuthenticationState.unauthenticated(e.toString()));
        }
      }
    });

    on<SignOutRequested>((event, emit) async {
      emit(AuthenticationState.unknown());
      emit(AuthenticationState.unauthenticated("Session cerrada"));
    });
  }
}
