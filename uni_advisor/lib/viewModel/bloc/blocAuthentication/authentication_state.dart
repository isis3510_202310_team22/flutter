part of 'authentication_bloc.dart';

enum AuthenticationStatus { authenticated, unauthenticated, unknow }

class AuthenticationState extends Equatable {
  const AuthenticationState._(
      {this.status = AuthenticationStatus.unknow,
      this.user = User.empty,
      this.message = ""});

  const AuthenticationState.unknown() : this._();

  const AuthenticationState.authenticated(User user)
      : this._(status: AuthenticationStatus.authenticated, user: user);

  const AuthenticationState.unauthenticated(String message)
      : this._(status: AuthenticationStatus.unauthenticated, message: message);

  final AuthenticationStatus status;
  final User user;
  final String message;

  @override
  List<Object?> get props => [status, user];
}
