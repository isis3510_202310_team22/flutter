part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class LoginRequested extends AuthenticationEvent {
  final String email;
  final String password;

  LoginRequested(this.email, this.password);
}

class SignUpRequested extends AuthenticationEvent {
  final String email;
  final String password;
  final String name;
  final String passwordx2;

  SignUpRequested(this.email, this.password, this.name, this.passwordx2);
}

class SignOutRequested extends AuthenticationEvent {}
