import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uni_advisor/model/models/user.model.dart';
import 'package:uni_advisor/model/serviceAdapter/scraper.dart';
import 'package:uni_advisor/services/camera.provider.dart';
import 'package:uni_advisor/services/firebase_storage.service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class ScrapingViewModel extends ChangeNotifier {
  String baseUrl;
  ScrapingViewModel(this.baseUrl);

  Future<List<Map<String, String>>> getElements() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      var scraper = Scraper(baseUrl);
      Future<List> images = scraper.getImages();
      Future<List> redirects = scraper.getRedirects();
      Future<List> titles = scraper.getTitles();
      Future<List> text = scraper.getTexts();
      await Future.wait([images, redirects, titles, text]);
      List imagesList = await images;
      List redirectsList = await redirects;
      List titlesList = await titles;
      List textsList = await text;
      List<Map<String, String>> ret = [];
      for (var i = 0; i < imagesList.length; i++) {
        Map<String, String> dict = {
          'Image': imagesList[i],
          'Title': titlesList[i],
          'Text': textsList[i],
          'Redirect': redirectsList[i]
        };
        ret.add(dict);
      }
      saveList(ret);
      return ret;
    } else {
      return await loadList();
    }
  }

  Future<void> saveList(List<Map<String, String>> list) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String jsonList = jsonEncode(list);
    await prefs.setString('myList', jsonList);
  }

  Future<List<Map<String, String>>> loadList() async {
    print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
    String? jsonList = prefs.getString('myList');
    print("ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc");
    if (jsonList == null) return [];

    List<dynamic> dynamicList = jsonDecode(jsonList);
    return dynamicList.map((item) => Map<String, String>.from(item)).toList();
  }
}
