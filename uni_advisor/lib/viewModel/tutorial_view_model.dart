import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:uni_advisor/model/models/teacher.dart';
import 'package:uni_advisor/model/models/tutorial.dart';
import 'package:uni_advisor/model/models/user.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';
import 'package:uni_advisor/viewModel/bloc/blocAuthentication/authentication_bloc.dart';

class TutorialViewModel {
  TutorialViewModel();
  final tutorial_repository =
      FirebaseServiceAdapter(FirebaseFirestore.instance, "tutorials");

  List<Tutorial> tutorials = [];

  Future<List<Tutorial>> getAllTutorials() async {
    tutorials = [];
    Future<List> future_tutorials = tutorial_repository.getAll();
    List json_tutorial = await future_tutorials;
    for (var tutorial in json_tutorial) {
      tutorials.add(Tutorial.fromJson(tutorial));
    }
    return tutorials;
  }

  Future<void> updateTutorial(Tutorial tutorial) async {
    tutorial_repository.update(tutorial.id, (tutorial).toJson());
  }

  Future<List<bool>> checkUserRegistration(
      List<Tutorial> tutorials, String user) async {
    List<bool> registered = [];
    var cont = 0;
    for (var tutorial in tutorials) {
      registered.add(false);
      for (var attendant in tutorial.attendants) {
        if (user == attendant) {
          registered[cont] = true;
        }
      }
      cont += 1;
    }
    return registered;
  }
}
