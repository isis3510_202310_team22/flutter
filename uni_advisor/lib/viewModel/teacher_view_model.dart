import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:uni_advisor/model/models/teacher.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';

class TeacherViewModel {
  TeacherViewModel();
  final teacher_repository =
      FirebaseServiceAdapter(FirebaseFirestore.instance, "teachers");
  final LocalStorage storage = LocalStorage('localstorage_app');

  List<Teacher> teachers = [];

  Future<List<Teacher>> getAllTeachers() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    print("inter");
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      Future<List> future_teachers = teacher_repository.getAll();
      List json_teachers = await future_teachers;
      for (var teacher in json_teachers) {
        teachers.add(Teacher.fromJson(teacher));
      }
      addTeachersLocalStorage(teachers);
      return teachers;
    } else {
      print("no inter");
      return getTeachersFromLocalStorage();
    }
  }

  void addTeachersLocalStorage(listTeachers) {
    final info = json.encode({'teachersList': listTeachers});
    storage.setItem('teachersList', info);
  }

  List<Teacher> getTeachersFromLocalStorage() {
    var rawList = [];
    List<Teacher> teachers = [];
    try {
      Map<String, dynamic> info = json.decode(storage.getItem('teachersList'));
      rawList = info['teachersList'];
      print(rawList);
      int topNumber = rawList.length;
      for (int i = 0; i < topNumber; i++) {
        teachers.add(Teacher.fromJson(rawList[i]));
      }
    } catch (e) {
      print("Falló al cargar datos");
    }
    print("Microoondas");
    return teachers;
  }

  Future<List> load() {
    return teacher_repository.getAll();
  }
}
