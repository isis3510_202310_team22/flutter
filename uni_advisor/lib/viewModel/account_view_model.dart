import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uni_advisor/model/models/user.model.dart';
import 'package:uni_advisor/model/repository/firebase_service_adapter.dart';
import 'package:uni_advisor/model/repository/user_repository.dart';
import 'package:uni_advisor/services/camera.provider.dart';
import 'package:uni_advisor/services/firebase_storage.service.dart';

class AccountViewModel extends ChangeNotifier {
  bool _loading = false;
  User _user = User.empty;
  bool _fillFields = true;
  bool _imageLoading = false;
  File _image = File('');
  bool _connected = true;

  bool get loading => _loading;
  User get user => _user;
  bool get fillFields => _fillFields;
  bool get imageLoading => _imageLoading;
  File get image => _image;
  bool get connected => _connected;

  //FirebaseRepository userProvider = FirebaseRepository(FirebaseFirestore.instance, "users");
  FirebaseStorageService storageService = FirebaseStorageService();
  UserRepository userRepository = UserDefaultRepository(
      userProvider:
          FirebaseServiceAdapter(FirebaseFirestore.instance, "users"));
  CameraProvider cameraProvider = CameraProvider();

  AccountViewModel();

  setLoading(loading) async {
    _loading = loading;
    notifyListeners();
  }

  setImageLoading(loading) async {
    _imageLoading = loading;
    notifyListeners();
  }

  setUser(User user) {
    _user = user;
  }

  setConnected(connectivity) async {
    _connected = connectivity;
  }

  setImage(File newImage) async {
    _image = newImage;
  }

  setFillFields(fill) async {
    _fillFields = fill;
  }

  getUser(String emailId) async {
    setLoading(true);
    var theUser = await userRepository.getUser(emailId);
    setUser(theUser);
    setLoading(false);
  }

  Future selectImage(ImageSource source) async {
    File? theImage = await cameraProvider.pickImage(source);
    if (theImage != null) {
      _image = theImage;
      setImageLoading(true);
      storageService.uploadFile('pps', _image).then((value) {
        user.ppUrl = value;
        setUser(user);
        userRepository.saveUser(user);
      }).catchError((error) {
        print(error);
      }).whenComplete(() {
        setImageLoading(false);
      });
    }
  }

  saveUser(String phoneNumber, String university, String career,
      String semester) async {
    user.phoneNumber = phoneNumber;
    user.university = university;
    user.career = career;
    user.semester = semester;

    userRepository.saveUser(user);
  }
}
