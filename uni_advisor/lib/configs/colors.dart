import 'dart:async';

import 'package:flutter/material.dart';
import 'package:light/light.dart';
import 'package:uni_advisor/services/light_provider.dart';

class AppColors {
  String _luxString = 'Unknown';
  static late Light _light;
  static late StreamSubscription _subscription;

  static Color primary =
      Color.fromRGBO(93, 169, 167, 1); //Usa para todo lo primario
  static Color secondary = Color.fromRGBO(245, 245, 245, 1);
  static Color accent = Color.fromRGBO(139, 132, 121, 1);
  static Color primaryText = Color.fromRGBO(0, 0, 0, 1);
  static Color secondaryText = Colors.white;
  static Color negativeSpace = Color.fromRGBO(255, 255, 255, 1); //Fonde
  static Color primaryDark = Color.fromRGBO(67, 114, 113, 1);
  static Color appBarColor = Colors.transparent;
  static Color loginText = Color.fromRGBO(255, 255, 255, 1);
  static Color cardColors = Color.fromRGBO(255, 255, 255, 1);
  static Color microwavecolor = Color.fromRGBO(67, 114, 113, 1);

  static void onData(int luxValue) async {
    if (true) {
      primary = Color.fromRGBO(93, 169, 167, 1);
      secondary = Color.fromRGBO(245, 245, 245, 1);
      accent = Color.fromRGBO(250, 242, 231, 1);
      primaryText = Color.fromRGBO(10, 9, 8, 1);
      loginText = Color.fromRGBO(255, 255, 255, 1);
      negativeSpace = Color.fromRGBO(255, 255, 255, 1);
      primaryDark = Color.fromRGBO(67, 114, 113, 1);
      appBarColor = Color.fromRGBO(93, 169, 167, 1);
      microwavecolor = Color.fromRGBO(67, 114, 113, 1);
    } else {
      primary = Color.fromRGBO(19, 64, 116, 1);
      secondary = Color.fromRGBO(19, 49, 92, 1);
      accent = Color.fromRGBO(11, 37, 69, 1);
      primaryText = Color.fromRGBO(255, 255, 255, 1);
      loginText = Color.fromRGBO(255, 255, 255, 1);
      negativeSpace = Color.fromRGBO(30, 30, 30, 1);
      primaryDark = Color.fromRGBO(19, 49, 92, 1);
      appBarColor = Color.fromRGBO(19, 49, 92, 1);
      cardColors = Color.fromRGBO(150, 150, 150, 1);
      microwavecolor = Color.fromRGBO(255, 255, 255, 0.5);
    }
  }

  void stopListening() {
    _subscription.cancel();
  }

  static void startListening() {
    _light = new Light();
    try {
      _subscription = _light.lightSensorStream.listen(onData);
    } on LightException catch (exception) {
      print(exception);
    }
  }

  static void updateColors() {
    startListening();
  }
}
